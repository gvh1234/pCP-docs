## piCorePlayer Documentation

This is main repository for piCorePlayer Documentation.

## More information

- [piCorePlayer Documentation](https://docs.picoreplayer.org/)
- [piCorePlayer Documentation - Forum](https://forums.slimdevices.com/showthread.php?112996-piCorePlayer-Documentation)
- [How to submit documents](https://docs.picoreplayer.org/publishing/)
