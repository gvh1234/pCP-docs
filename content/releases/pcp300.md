---
title: piCorePlayer 3.00
description:
date: 2016-08-21
author: pCP Team
weight: 1
pcpver: "3.00"
toc: true
draft: false
---

### Note

If you have used pCP 3.00 before this date, please start from fresh again with a new download of pCP 3.00.

### Download

Please try the new version [piCorePlayer 3.00](https://repo.picoreplayer.org/insitu/piCorePlayer3.00/piCorePlayer3.00.img).

We had some nasty bugs which now are fixed.

### Changes

- HiFiBerryDACs are now working.
- Buffer settings are now working.
- User commands are now working.

### IMPORTANT

We have problems doing insitu updates from previous versions to the current pCP 3.00. So please download pCP 3.00 image and burn to your SD card.

### To keep your current settings

- Using your current version of pCP save your config to a USB-flash drive (from the [Main] web page use [Save to USB]).
- Keep the USB flash drive attached.
- Boot your pCP 3.00---and it will use your current settings in pCP 3.00.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
