---
title: piCorePlayer 1.18b
description:
date: 2014-11-02
author: pCP Team
weight: 1
pcpver: "1.18b"
toc: true
draft: false
---

If you were using a HiFiBerryAMP (the amplifier card from HiFiBerry) please update to **piCorePlayer 1.18b**, otherwise your card was not recognised after a reboot.

It has been fixed now.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
