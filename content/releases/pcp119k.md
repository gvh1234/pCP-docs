---
title: piCorePlayer 1.19k
description:
date: 2015-05-14
author: pCP Team
weight: 1
pcpver: "1.19k"
toc: true
draft: false
---

Hi all piCorePlayer users.

**Note: In this version of piCorePlayer a new password is used.**

User: **tc**
Password: **piCore**

We have a new version ready **piCorePlayer 1.19k**.

It is mostly a bug fix version but Greg have developed a new option that allow you to set a static IP address for your piCorePlayer. This has been requested frequently, so now the first part is ready. For now it is only for wired networks, so if you use Wifi you can't set a static IP-address. We hope to have such an option later.

### Changes

- Fixed the issue with not being able to save your changes.
- Fixed the issue with Jivelite becoming unresponsive after changing the skin.
- Fixed the Squeezelite logging to tmp directory issue.
- Shortened boot time.
- Cleaned up boot messages.
- Added a "Static IP" setting on the main page for wired networks.

Can be downloaded [here.](/downloads/)

The pCP Team<br>
Paul, Greg, Ralphy and Steen
