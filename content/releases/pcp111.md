---
title: piCorePlayer 1.11
description:
date: 2014-02-01
author: pCP Team
weight: 1
pcpver: "1.11"
toc: true
draft: false
---

Hi all.

I have a new version ready... get it from the [Downloads](/downloads/) section.

It is **piCorePlayer 1.11**. It is a major rework as it now has a build in web server, so you can control the settings via your browser.

Simply use the IP address of the Raspberry Pi and port number 8077. Something like 192.168.1.24:8077 And then it should be fairly simple to change the settings of your player.
I have only used a very small web server with CGI support and bash scripting, so it is very lightweight and I haven't seen any negative impact on a running system.
It has support for the I2S HiFiBerry DAC which is based on a 5102 chip and a recent report stated that it also supported another I2S DAC based on the ES9023 chip.
The I2S DACs are working really well, as they don't connect via USB which sometimes can be problematic on a Raspberry Pi.
I suspect that more I2S DACs will become available, and if I have access to them, I will try to support them in piCorePlayer.

### Changes

### HDMI

For those of you using HDMI, I have incorporated the suggestions by Burki:

- hdmi_drive=2
- hdmi_force_hotplug=1
- hdmi_force_edid_audio=1
- hdmi_ignore_edid=0xa5000080

And also the suggestion from Pete:

- amixer cset numid=3 2

So hopefully the HDMI output will be fixed. Also I have patched the kernel, so now I think that you should be able to get 192kHZ via HDMI again. But please test this HDMI stuff, because I'm not using HDMI myself and it is difficult for me to test it properly.

### New configuration option:

You can log into piCorePlayer as usual (user:tc password:nosoup4u) and type picoreplayer. Then the old configuration script is still there (here you can also disable the web-server if you don't want it). However, I would like to hear your opinion on the new configuration via your browser: use IP address:8077 something like 192.168.1.23:8077 (this is my IP address, yours will be different).

Configuration via Browser

### Tweaks section:

Here you can play with changing the number of NR-packs. Change the hostname of your player and change your overclocking settings (it can have an effect if you are using USB DAC's).

### About section:

- Here you can easily see what version of piCorePlayer and Squeezelite you are using.
- Also the content of your config.cfg file is shown.

### New option:

In order to make it easier to port your settings to another piCorePlayer (and also to change your settings offline) I have made a file "newconfig.cfg" which you can download from here: [newconfig.cfg](https://sourceforge.net/projects/picoreplayer/files/newconfig.cfg/download). If you place this file on a USB stick that is present during booting of your piCorePlayer, the file newconfig.cfg, will be copied and used as the config.cfg file on the piCorePlayer.

It has been suggested that I should put this file in the FAT formatted partition on the SD card, and I might do that in the next version. But for now you need to put it on a USB stick that is present during booting.

Please report all the bugs, and suggestions for improvement.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
