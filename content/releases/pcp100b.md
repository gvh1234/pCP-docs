---
title: piCorePlayer 1.00b
description:
date: 2013-06-13
author: pCP Team
weight: 1
pcpver: "1.00b"
toc: true
draft: false
---

A new test version is ready. It is build on kernel 3.8.13+ and using the special fiq_split branch. In addition I have tweaked the kernel, so that it might be possible t o use 192kHz as max sample rate via HDMI. Before the max rate was 48kHz. I can't test this so please report back.

Get it from here: TestVersion 1.0 of the piCorePlayer (not available  any more)

The pCP Team<br>
Paul, Greg, Ralphy and Steen
