---
title: piCorePlayer 1.19l
description:
date: 2015-05-17
author: pCP Team
weight: 1
pcpver: "1.19l"
toc: true
draft: false
---

New **piCorePlayer 1.19l** ready for both the original RPi as well as the new RPi2.

### Changes

- Timezone settings survives an update - FIXED.
- Increase in boot speed (if not using Wifi).

### Issues

- Still not saving your custom ALSA settings after an update - we have it fixed now for the next version.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
