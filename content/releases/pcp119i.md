---
title: piCorePlayer 1.19i
description:
date: 2015-04-15
author: pCP Team
weight: 1
pcpver: "1.19i"
toc: true
draft: false
---

New versions **piCorePlayer 1.19i** and **piCorePlayer 1.19i_RPi2** are available from the download page.

### Changes

- Fixed the problem with Wifi and RPiA+ cards (did not load firmware).
- To set up an only Wifi based system download the newconfig.cfg file from here: [Downloads](/downloads/). Then change the Wifi section so it fits your setup and save it to your SD card boot partition (in a windows computer just save it to the SD card as it is the only partition visible in windows). Next boot the Raspberry Pi from this SD card.
- Improved booting speed.
- Added Jivelite support on the Tweaks page - thanks to Ralphy for providing the Jivelite package.
- On **armv6 boards** (the older RPi's) HDMI streaming now supports high sample rates.
- On **armv7 boards** (the new RPi2) HDMI streaming still only support up to 48kHZ. Sorry for that but my RPi2 compiling computer crashed. Will be fixed later.

For Wifi setup on a RPi A+ this section of newconfig.cfg needs changing:
```
########################## Edit the variables below to setup a Wifi based system without LAN access
# Change Wifi to "on". Valid options for Encryption are "WEP", "WPA" or "OPEN"
WIFI="off"
SSID="wireless"
PASSWORD="password"
ENCRYPTION="WPA"
# End Wifi setup
##########################
```
The pCP Team<br>
Paul, Greg, Ralphy and Steen
