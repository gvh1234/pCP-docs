---
title: piCorePlayer 1.21g
description:
date: 2015-10-31
author: pCP Team
weight: 1
pcpver: "1.21g"
toc: true
draft: false
---

Finally, we succeeded to support the new official Raspberry Pi 7" touch screen.

With the help from Ralphy (from the Squeezebox forum) and bmarkus (from the piCore team) we managed to get the new 7" Touch Screen working with piCorePlayer, so now you can use Jivelite and piCorePlayer will be almost like a original Squeezebox Touch.

So please try **piCorePlayer 1.21g**.

### Changes

- Improved the mount/unmount behaviour.
- Fixed a problem with selecting audio output in RPiA.
- Numerous small bug fixes and improvements in web pages.
- Added option to control output volume by ALSA.
- Fixed the problem that all dtovelays in config.txt was wrongly removed.
- Added unmute via GPIO pin 22 - used for IQaudIO AMP - and Digi AMP+.
- Added touch function for the official RPi 7" touch screen.
- Fixed the calibration issue with the screen.
- Added rotate screen option on the [Tweaks] page.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
