---
title: piCorePlayer 1.18a
description:
date: 2014-10-27
author: pCP Team
weight: 1
pcpver: "1.18a"
toc: true
draft: false
---

Small update to **piCorePlayer 1.18a**.

### Changes

- Added support for the [HiFiBerryAMP 2x25W amplifier card](http://www.hifiberry.com/amp/).

![hifiberryamp-square](http://www.hifiberry.com/wp-content/uploads/2014/05/hifiberryamp-square-441x441.jpg)

The pCP Team<br>
Paul, Greg, Ralphy and Steen
