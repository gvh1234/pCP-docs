---
title: piCorePlayer 2.00
description:
date: 2016-01-14
author: pCP Team
weight: 1
pcpver: "2.00"
toc: true
draft: false
---

Hi all.

We are ready to release a major update to **piCorePlayer** - so we decided to call it [version 2.00](/downloads/) and start a [new thread](http://forums.slimdevices.com/showthread.php?105018-Announce-piCorePlayer-2-00). The [old thread](http://forums.slimdevices.com/showthread.php?97803-piCorePlayer-Squeezelite-on-Microcore-linux-An-embedded-OS-in-RAM-with-Squeezelit) is huge and has been read more than 700.000 times, therefore it is time for a [new thread](http://forums.slimdevices.com/showthread.php?105018-Announce-piCorePlayer-2-00). So thank you all for your continued interest and support.

### Major changes are

- Only **one version of piCorePlayer**, but it can now run on all known Raspberry Pi boards. So you can use the same piCorePlayer image for all your players.
- **Shairport-sync** is supported and can be enabled from the "tweaks" page. It will automatically show on your iDevice (iPad or iPhone) with the name you have given your piCorePlayer. This means that you now can stream from your iDevices to piCorePlayer. It will automatically switch from Squeezelite to shairport and vice versa. Just stop/pause either Squeezelite or Shairport.
- **piCorePlayer menu applet in Jivelite.** This means that you will have a specific piCorePlayer menu in Jivelite. From here you can reboot and save eventual changes from within Jivelite. This menu is installed from your running LMS server. At the bottom of the Plugin page in LMS you will need to add piCorePlayer repro: http://picoreplayer.sourceforge.net/repo.xml Save.
- Then you go to Jivelite on the piCorePlayer. Settings - Advanced - Applet installer - piCorePlayer menu - Install.
- Next use the arrow in the top left corner to get to the main menu. Choose "quit" in the bottom of the screen (this will restart Jivelite - so the piCorePlayer menu is visible).
- Go to Settings - choose piCorePlayer and select "Save settings to SD card"
- Kernel updated to 4.1.13 and piCore to version 7.0.

### Notes

Because this version is so different from the others it is not possible to use the "In situ" upgrade/downgrade system to go down from version 2.00 to a previous version. If you for some reason want to use an older version you will have to download the previous version and burn it to your SD card.

Please try it out. For this version Ralphy has been a tremendous help and he build the shairport-sync with all its dependencies. Kolossos made the piCorePlayer menu script.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
