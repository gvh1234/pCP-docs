---
title: piCorePlayer 4.0.0
description:
date: 2018-09-01
author: pCP Team
weight: 1
pcpver: "4.0.0"
toc: true
draft: false
---

The pCP Team has released an new version of piCorePlayer.

### Here are the major changes

- Launching new web site---see [www.picoreplayer.org](https://www.picoreplayer.org/).
- Kernel 4.14.56
- AudioCore kernel 4.14.56-rt34. All RPi processors supporting Realtime Kernels.
- RPi Firmware 2018/07/17
- Completely rebuilt Wifi. Web interface with hidden Wifi, special characters.
- Added driver for rtl8188eu chipset.
- Static IP support for Ethernet or Wifi.

### Repo updates

- Update to Squeezelite v1.9.0-1111
- Updated Bluez to 5.50
- Updated LMS binaries (flac and sox are now significantly faster, upgrade perl cyrpto libraries).

### DAC additions

- Allo Katana.
- Google voiceHat.
- pCP default kernel now supports 384kHz on a good number of DACs.

### Download

New images or insitu update are available. The released versions can be downloaded from here.

- [piCorePlayer 4.0.0 - Standard Version](https://repo.picoreplayer.org/insitu/piCorePlayer4.0.0/piCorePlayer4.0.0.zip)
- [piCorePlayer 4.0.0 - Experimental RealTime Kernel](https://repo.picoreplayer.org/insitu/piCorePlayer4.0.0/piCorePlayer4.0.0-Audio.zip)

The pCP Team<br>
Paul, Greg, Ralphy and Steen
