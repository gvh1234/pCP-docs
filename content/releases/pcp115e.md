---
title: piCorePlayer 1.15e
description:
date: 2014-05-07
author: pCP Team
weight: 1
pcpver: "1.15e"
toc: true
draft: false
---

Hi

Now I think we have nailed the problem with the latest USB development. In **piCorePlayer 1.15e** the USB audio is now as good as with piCorePlayer 1.15c and the I2S audio cards problems and the analog audio is fixed as well.

So please try **piCorePlayer 1.15e**.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
