---
title: piCorePlayer 1.05
description:
date: 2013-07-30
author: pCP Team
weight: 1
pcpver: "1.05"
toc: true
draft: false
---

**piCorePlayer 1.05** is now ready.

It plays all sample rates from 44.1 to 192kHz via HDMI. Please use this string if you use HDMI: -a ::32:0

I have included rt2800.bin, so hopefully more Wifi devices are supported.

I have updated ALSA to version 1.0.27.2.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
