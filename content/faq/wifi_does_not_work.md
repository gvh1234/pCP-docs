---
title: Wifi does not work
description:
date: 2020-12-05
author: pCP Team
weight: 10
pcpver: "6.1.0"
draft: false
categories:
- FAQ
tags:
- Wifi
---

## A list of common Wifi issues

- Wrong password.
- Wrong SSID.
- Wifi adapter does not support Wifi channel selected on Wifi router.
- USB Wifi adapter and built-in Wifi adapter both trying to run at the same time.
- Some audio cards interfere with Raspberry Pi built-in Wifi adapters.
- Wifi drivers are gradually changing from "wext" to "nl80211" standard.
- Kernel upgrades can break USB Wifi drivers. Unfortunately, we can not test all USB Wifi drivers prior to each piCorePlayer release.

{{< card style="warning" >}}Please report any issue.{{< /card >}}

## Diagnostics

- Check the Wifi MAC and IP addresses have been set by looking at [Wifi settings] > "Wifi information". For example:
```
	Wifi MAC: dc:a6:32:00:2a:4d
	Wifi IP: 192.168.1.110
```
- For more Wifi diagnostics information click on [Wifi settings] > "Set Wifi configuration" > [Dignostics].


## More information

- [Setup Wifi](/how-to/setup_wifi/)
- [Setup Wifi without ethernet](/how-to/setup_wifi_on_pcp_without_ethernet/)
- [Example wpa_supplicant configuration file](http://w1.fi/cgit/hostap/plain/wpa_supplicant/wpa_supplicant.conf)
- [WPA Supplicant](http://w1.fi/wpa_supplicant/)
- [USB wifi adapters - historical information](https://sites.google.com/site/picoreplayer/wifi-dongles)
