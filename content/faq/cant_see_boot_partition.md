---
title: Can't see the boot partition
description:
date: 2020-10-29
author: pCP Team
weight: 30
pcpver: "6.1.0"
toc: true
draft: false
categories:
- FAQ
---

{{< lead >}}
By default, after booting piCorePlayer, the boot partition is intentionally unmounted. This helps protect the partition from corruptions. The boot partition contains the files for getting piCorePlayer up and running so it is not usually needed after the initial startup.
{{< /lead >}}

So to get access to the boot partition it will need to be remounted.

##### For SD card

$ `mount /mnt/mmcblk0p1`

{{< card style="warning">}}If you are not using a SD card, change path to suit your boot device.{{< /card >}}

##### piCorePlayer CLI shortcut

$ `m1`


## More information

- [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/)
- [piCorePlayer aliases](/information/pcp_aliases/)
