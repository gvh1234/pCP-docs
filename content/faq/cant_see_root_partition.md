---
title: Can't see the root partition
description:
date: 2020-10-29
author: pCP Team
weight: 40
pcpver: "6.1.0"
toc: true
draft: false
categories:
- FAQ
---

{{< lead >}}
Windows PCs can't see the Linux partition on the SD card (or USB device).
{{< /lead >}}

Windows will work with partitions formatted as `Win95 FAT32 (LBA)`, so it will see the `boot` partition `/dev/mmcblk0p1`, but the Linux formatted `root` partition `/dev/mmcblk0p2` is invisible. For all intents and purposes the SD card appears to be only 64.0M in size.

If access to the `root` partition is required, there are 2 options:
- use a computer with Linux type OS.
- load additional software onto the Windows PC.

{{< card style="danger" >}}
Depending on the software loaded on your Windows PC, you may be get a series of messages when you plug your SD card in. Always:
- Ignore formatting requests.
- Ignore anti-virus warnings.
- Ignore requests to do backups.
{{< /card >}}


## More information

A standard piCorePlayer image contain 2 partitions:

```text
Device     Boot   StartCHS    EndCHS      StartLBA     EndLBA    Sectors   Size  Id  Type
/dev/mmcblk0p1    128,0,1     127,3,16        8192     139263     131072  64.0M   c  Win95 FAT32 (LBA)
/dev/mmcblk0p2    1023,3,16   1023,3,16     139264    4235263    4096000  2000M  83  Linux
```

- [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/)
- [piCorePlayer aliases](/information/pcp_aliases/)
