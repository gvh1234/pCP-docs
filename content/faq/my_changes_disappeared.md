---
title: My changes disappeared
description:
date: 2020-10-29
author: pCP Team
weight: 20
pcpver: "6.1.0"
toc: true
draft: false
categories:
- FAQ
tags:
- Backup
---

{{< lead >}}
piCorePlayer uses piCore which is predominately a RAM based system, so nearly everything is stored in RAM. If you modify or add files it is highly likely that you are doing the changes in RAM. So after a reboot your changes may disappear.
{{< /lead >}}

To make changes persistent you need to do one of the following:

- add/modify files in your `/home/tc` or `/opt` directories, then do a backup---see [piCorePlayer backup](/information/pcp_backup/). The `home` and `opt` directories are already included in `/opt/.filetool.lst` so they get included in a backup.
- add/modify files, then edit `/opt/.filetool.lst` to include these files, then do a backup---see [piCorePlayer backup](/information/pcp_backup/).
- add files directly on the `root` partition of your storage device (ie. `/mnt/mmcblk0p2`).
- add files directly on the `boot` partition of your storage device (ie. `/mnt/mmcblk0p1`).

{{< card style="warning" >}}
The boot partition is used during the boot process and by default is normally unmounted.
{{< /card >}}

## More information

- [piCorePlayer backup](/information/pcp_backup/)
- [Into the Core (pdf)](http://www.tinycorelinux.net/corebook.pdf)
