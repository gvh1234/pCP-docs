---
title: Setup Argon One case for RPi4B
description: Setup the Argon One case for the Raspberry Pi 4B
date: 2021-02-05
author: carsten_h
weight: 140
pCPver: "7.0.0"
toc: true
draft: false
categories:
- Projects
tags:
- Argon
---

{{< lead >}}
In this little How To I will describe what is necessary to use the Fan and the Power button of the [Argon One Case](https://www.argon40.com/catalog/product/view/id/52/s/argon-one-raspberry-pi-4-case/category/4/) for the Raspberry Pi 4B together with a piCorePlayer (7.x) installation.
{{< /lead >}}

![Argon One Case](argon-one-rpi4b.jpg)

{{< card style="info" >}}
I assume that the Raspberry Pi 4B is installed inside the case and piCorePlayer (7.x) is installed. Also you know the IP address or the name of this RPi.
{{< /card >}}

##### Step 1 - ssh into RPi

- For the first step we need to login to the RPi via ssh---see [Access piCoreplayer via ssh](/how-to/access_pcp_via_ssh/).
- This can be done on the Mac with the normal Terminal.app or on a Windows PC with eg. putty. I will describe here only the way to get to the RPi for a Mac.
- So start the Terminal.app, then type `ssh tc@<IP or name of the Pi>` (without the "<" and ">"!)
- You will then be asked for the password of the user `tc` which is `piCore` if you didn't change it.

##### Step 2 - Create argononed.py

- Then we create the daemon file which is later used to watch for the temperature and for the Power button presses, type `vi argononed.py`.
- Once `vi` is started (you see only a few lines with "~" and at the end the filename and a few other things) you press `i`.
- Then you change back to this browser window and select the text of the following python file:

{{< code lang="python" >}}
#!/usr/bin/python
import smbus
import RPi.GPIO as GPIO
import os
import time
from threading import Thread
bus = smbus.SMBus(1)
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
shutdown_pin=4
GPIO.setup(shutdown_pin, GPIO.IN,  pull_up_down=GPIO.PUD_DOWN)
def shutdown_check():
	while True:
		pulsetime = 1
		GPIO.wait_for_edge(shutdown_pin, GPIO.RISING)
		time.sleep(0.01)
		while GPIO.input(shutdown_pin) == GPIO.HIGH:
			time.sleep(0.01)
			pulsetime += 1
		if pulsetime >=2 and pulsetime <=3:
			os.system("pcp rb")
		elif pulsetime >=4 and pulsetime <=5:
			os.system("pcp sd")
def get_fanspeed(tempval, configlist):
	for curconfig in configlist:
		curpair = curconfig.split("=")
		tempcfg = float(curpair[0])
		fancfg = int(float(curpair[1]))
		if tempval >= tempcfg:
			return fancfg
	return 0
def load_config(fname):
	newconfig = []
	try:
		with open(fname, "r") as fp:
			for curline in fp:
				if not curline:
					continue
				tmpline = curline.strip()
				if not tmpline:
					continue
				if tmpline[0] == "#":
					continue
				tmppair = tmpline.split("=")
				if len(tmppair) != 2:
					continue
				tempval = 0
				fanval = 0
				try:
					tempval = float(tmppair[0])
					if tempval < 0 or tempval > 100:
						continue
				except:
					continue
				try:
					fanval = int(float(tmppair[1]))
					if fanval < 0 or fanval > 100:
						continue
				except:
					continue
				newconfig.append( "{:5.1f}={}".format(tempval,fanval))
		if len(newconfig) > 0:
			newconfig.sort(reverse=True)
	except:
		return []
	return newconfig
def temp_check():
	fanconfig = ["65=100", "60=55", "57=30", "55=10", "30=0"]
	tmpconfig = load_config("/home/tc/argononed.conf")
	if len(tmpconfig) > 0:
		fanconfig = tmpconfig
	address=0x1a
	prevblock=0
	while True:
		temp = os.popen("vcgencmd measure_temp").readline()
		temp = temp.replace("temp=","")
		val = float(temp.replace("'C",""))
		block = get_fanspeed(val, fanconfig)
		if block < prevblock:
			time.sleep(30)
		prevblock = block
		try:
			bus.write_byte(address,block)
		except IOError:
			temp=""
		time.sleep(30)
try:
	t1 = Thread(target = shutdown_check)
	t2 = Thread(target = temp_check)
	t1.start()
	t2.start()
except:
	t1.stop()
	t2.stop()
	GPIO.cleanup()
{{< /code >}}

- Then you press "Command-C" (on the PC it would be "Control-C") and change back to the Terminal.app window.
- There you simply press "Command-V" (on the PC it would be "Control-V").
- Now you should see the python file inside your editor.
- Now press `ESC`, then type `:wq`.

##### Step 3a - Create argononed.conf

- Then we create the fan configuration file which is later used to control at which temperature the fan should be set which fan speed, type `vi argononed.conf`.
- Once it is started (you see only a few lines with "~" and at the end the filename and a few other things) you press `i`.
- Then you change back to this browser window and select the text of the following configuration file:

{{< code lang="shell" >}}
#
# Argon One Fan Configuration
#
# List below the temperature (Celsius) and fan speed (in percent) pairs
# Use the following form:
# min.temperature=speed
#
# Example:
# 30=0
# 55=10
# 57=30
# 60=55
# 65=100
#
# NOTE: Lines beginning with # are ignored
#
# Start below:
30=0
55=10
57=30
60=55
65=100
{{< /code >}}

- Then you press "Command-C" (on the PC it would be "Control-C") and change back to the Terminal.app window.
- There you simply press "Command-V" (on the PC it would be "Control-V").
- Now you should see the python file inside your editor.
- Now press `ESC`, then type `:wq`.

##### Step 3b - Change configuration values (optional)

- If you want you can change the temperatures and the corresponding percent values of the fan in the configuration file.
- You have there the temperature in °C and after the "=" the percent value of the fan. You can experiment with them.

##### Step 4 - Close terminal

- You can now close the Terminal.app, as you have done everything that is needed to be done on the shell.

##### Step 5 - Select [Beta] mode

- Now all other things have to be done in a browser, so open one and type `http://<IP or name of your RPi>`
(without the "<" and ">"!).
- On the piCorePlayer page choose [Main Page]:

	![Main Page](main-page.png)

- At the bottom of the page choose [Beta]:

	![Beta](beta.png)

##### Step 6 - Download required extensions

- Then choose [Extensions]:

	![Extensions](extensions.png)

- On the following [Information] page wait until all the five green checkmarks display and then choose [Available]:

	![Available](available.png)

- There you have to:
    - Choose `rpi-vc.tcz` then press on [Load].
    - Choose `i2c-tools.tcz` then press on [Load].
    - Choose `python3.8-rpi-gpio.tcz` then press [Load].
    - Choose `python3.8-smbus.tcz` then press [Load].
- If some of these extension are already installed, it is OK, you don't have to load them again.

##### Step 7 - Add User Commands

- Then you have to choose [Tweaks] at the top:

	![Tweaks](tweaks.png)

- On that page input the following "User Commands" into the appropriate fields:
    - User Command #1: `modprobe i2c-dev`
    - User Command #2: `python3 /home/tc/argononed.py`
- And now press [Save] (at this place the argononed.py script and the argononed.conf file are also saved to the boot device):

	![User commands](user-commands.png)

##### Step 8 - Reboot

- Now [Reboot] via the [Main Page] and now everything should be working:

	![Reboot](reboot.png)

- Short press (ca. 3 seconds) of the Power button: Shutdown
- Double press of the Power button: Reboot
- Long press of the Power button: Hard switching the RPi off

##### Step 9 - Completed

- The fan should be working also, when the temperature reaches the values of the table in the python script/the configuration file if there is one.
- If you later make changes to the script `argononed.py` or to the `argononed.conf` file, you have to use the [Backup] button on the [Main Page] again, to save the changes to your boot device!

	![Backup](backup.png)

- If you make changes to the `argononed.conf` file, they will be loaded after 30 seconds and you will hear the changes in fan speed directly.

## Here is one addition from @chill

If you want that every time the Shutdown or the Reboot is done via the power button the actual configuration of piCorePlayer is saved, you can change theses lines at the beginning of the argononed.py script:

{{< code lang="python" >}}
def shutdown_check():
	while True:
		pulsetime = 1
		GPIO.wait_for_edge(shutdown_pin, GPIO.RISING)
		time.sleep(0.01)
		while GPIO.input(shutdown_pin) == GPIO.HIGH:
			time.sleep(0.01)
			pulsetime += 1
		if pulsetime >=2 and pulsetime <=3:
			os.system("pcp rb")
		elif pulsetime >=4 and pulsetime <=5:
			os.system("pcp sd")
{{< /code >}}

into:

{{< code lang="python" >}}
def shutdown_check():
	while True:
		pulsetime = 1
		GPIO.wait_for_edge(shutdown_pin, GPIO.RISING)
		time.sleep(0.01)
		while GPIO.input(shutdown_pin) == GPIO.HIGH:
			time.sleep(0.01)
			pulsetime += 1
		if pulsetime >=2 and pulsetime <=3:
			os.system("pcp br")
		elif pulsetime >=4 and pulsetime <=5:
			os.system("pcp bs")
{{< /code >}}


## More information

- [Argon One Case](https://www.argon40.com/catalog/product/view/id/52/s/argon-one-raspberry-pi-4-case/category/4/)
- [Development thread](https://forums.slimdevices.com/showthread.php?113575-How-To-Using-the-Argon-One-case-for-the-Pi-4B-together-with-piCorePlayer-(7-x)&p=1002008&viewfull=1#post1002008)
- [Documentation thread](https://forums.slimdevices.com/showthread.php?112996-piCorePlayer-Documentation&p=1004893&viewfull=1#post1004893)
- [Argon One Replacement Daemon](https://www.raspberrypi.org/forums/viewtopic.php?f=29&t=275713) (FYI: Not working on pCP)
- [Argon One Case with IR receiver and LIRC](https://www.raspberrypi.org/forums/viewtopic.php?p=1803426#p1803426)
