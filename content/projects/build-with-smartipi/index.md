---
title: Setup SmartiPi Touch case
description: Build a player with the SmartiPi Touch case
date: 2021-02-07
author: gvh1234
weight: 150
pCPver: "7.0.0"
toc: true
draft: false
categories:
- Projects
tags:
- SmartiPi
---

{{< lead >}}
This page shows various recipes for using the official touchscreen and SmartiPi cases from [SmartiCase](https://smarticase.com/) to make a really great looking player. There are 3 different players shown here and the various options and tradeoffs are discussed along with shopping lists.
<br />
<br />
My goal is simple, to build a player that I do not have to apologise for.
{{< /lead >}}

### SmartiPi Touch Pro with RPi3B+ and Justboom DigiHat

This player uses the latest model, the Touch Pro. It is easily the best looking and most suitable case to make a great looking player. The layout allows for everything to be hidden and adapted to any combination of hardware.

![Complete player with SmartiPi Touch Pro](2021-01-0908.39.34.jpg)

![Side view of SmartiPi Touch Pro](2021-01-0908.41.42.jpg)

##### The Parts List

- SmartiPi Pro
- Official Touch Screen
- Raspberry Pi 3B+
- Official power supply to match
- Add-on switch to that power supply so I can turn it off easily (see notes below)
- MicroSD
- Justboom DigiHat - I run digital output on this player
- SPDIF cable - thin so it behaves well
- [Flirc V2 Remote Receiver](https://flirc.tv/flirc-usb)
- USB2 Extension cable to run Flirc outside
- Right angle USB adapter cable
- piCorePlayer


##### Notes on this

Here are the lessons I learned from this one:

- Install the power cables for the fan to the screen (step 1 of the SmartiPi instructions) even if you do not intend to use them immediately. You will have to dismantle the whole thing to put them in afterwards. Since the photos were taken I have added the fan
- The standard size back of the case is all you need for a standard HAT
- I get most parts from [The Pi Hut](https://thepihut.com/) and the little bits from eBay
- I cut holes in the back of the case using a cheap, Dremel-style, tool. (not shown in the photos)
- SmartiPi supply an optional metal plate for the bottom to counter balance the screen. I nearly need it
- I use a Squeezebox remote
- You can easily use a RPi3A+ if you just want wifi. Cheaper, smaller.
- I added a switch to the power cable and then added a right-angle adapter. This combination resulted in a voltage drop enough to give me continuous warnings. The switch had to go. My advice - buy an official power supply with switch built in
- Once any of these machines are built the SD card is hard to access. I don't mind because I upgrade the software in place

##### Inside

Look carefully and you will notice that I have mounted the RPi deep inside the machine away from the sides. Since these photos were taken I have run a USB extension cable out of the bottom with the Flirc V2 receiver hidden in the hifi cabinet out of sight.

![Inside the case with SmartiPi Touch Pro](2021-01-0908.27.05.jpg)

![Inside the case #2 SmartiPi Touch Pro](2021-01-0908.27.14.jpg)


### SmartiPi Touch 2 with RPi4B and USB DAC

This player is in my office and I don't mind that it doesn't look so good because the sound is amazing.

![Touch 2 with RPi4B](2021-02-0715.23.05.jpg)

![Touch 2 with RPi4B rear view](2021-02-0715.22.43.jpg)

- Raspberry Pi 4B
- Heatsinks
- Official Touchscreen
- Official power supply
- Add-on switch for the power supply
- MicroSD
- Flirc V2 USB Receiver
- [Khadas Tone Board](https://www.khadas.com/tone1) DAC
- Case for the Khadas from [Audiophonics](https://www.audiophonics.fr/en/aluminium-boxes-cases/aluminum-case-for-khadas-tone-board-p-13626.html) 

My understanding is that a RPi4B is much better for a USB DAC than any other.

### SmartiPi Touch 1 with RPi3A+ and HiFiBerry DAC+

This one is a Touch 1 case I happened to pick up second hand. It uses a RPi3A+ which is a very overlooked option. It is shorter and hides really well behind this case. The Flirc V2 USB receiver does not show from the front making for a very good solution.

In this case I am using the [HiFiBerry](https://www.hifiberry.com/) DAC+ card with right-angle phono adapters picked up from eBay. Notice that the Pi is held in place by a piece of plastic that came with the SmartiPi. Hardly any screws here at all.

It is very easy to set up the WiFi just after you download piCorePlayer to the SD card.

![Touch 1 and RPi3A+](2021-02-0713.12.19.jpg)

![Touch 1 and RPi3A+ rear view](2021-02-0713.12.45.jpg)


### Tradeoffs and Lessons Learned

- A RPi4B is overkill for a pure player and runs hotter than a RPi3
- In the summer here in the UK the Touch Pro machine will definitely need the fan
- I run LMS on a dedicated RPi4B with attached SSD
- When looking for a remote to use with a Flirc, make sure it has play and pause buttons

### Links

- [My post on the Pro](https://forums.slimdevices.com/showthread.php?113637-SmartiPi-Touch-Pro-case-review)
- [HOWTO: Building and Installing the Raspberry Pi 3 "Touch" Audio Streamer.](http://archimago.blogspot.com/2017/03/howto-building-and-installing-raspberry.html) Where I first learned how to do this

