---
title: Add a Topping E30 USB DAC
description:
date: 2021-01-02
author: pCP Team
weight: 110
pcpver: "7.0.0"
toc: true
draft: false
categories:
- Projects
tags:
- USB
- DAC
---

{{< lead >}}
This project is to add a Topping E30 USB DAC to piCorePlayer.
{{< /lead >}}

![Topping E30](e30.png)

## What was used


### Hardware

- Raspberry Pi 4B - 1GB
- Official RPF 5.1V 3A PSU
- SanDisk Ultra 8GB SD card
- Topping E30 USB DAC---see [Topping E30](http://www.topping-audio.com/productinfo/434825.html)

### Software

- piCorePlayer 7.0.0---see [piCorePlayer Downloads](/downloads/)


### Network Diagram

<div class="mermaid">
	graph TD
		Router[Wifi router]
		Router --> |Wifi| RPi[RPi4B - pCP]
		RPi --- |USB 2| DAC[Topping E30]
		RPi --- |USB 2| DAC[Topping E30]
		DAC --> AMP[Amplifier]
		DAC --> AMP[Amplifier]
</div>

## Steps

##### Step 1 - Download pCP

- Download piCorePlayer---see [Download piCorePlayer](/how-to/download_picoreplayer/).

##### Step 2 - Create SD card

- Create an SD card with piCorePlayer image---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).

##### Step 3 - Setup Wifi

- Create wpa_supplicant.conf on boot partition---see [Setup Wifi without ethernet](/how-to/setup_wifi_on_pcp_without_ethernet/).
- Eject the SD card.

##### Step 4 - Connect Topping E30 USB DAC

- Connect the 2 USB cables from the RPi USB2 ports to Topping E30.
- Topping E30 was used with default settings.

##### Step 5 - Boot pCP

- Insert the SD card into the Raspberry Pi.
- Connect power cable.
- Turn the power on.

##### Step 6 - Determine the IP address

- Determine the IP address---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).

##### Step 7 - Access pCP GUI

- Type `http://<IP address>` into web browser, **or**
- Type `http://pcp.local` into web browser.

##### Step 8 - Select USB audio

- Select [Squeezelite Settings] > "Audio output device settings" > "USB audio".
- Click [Save].
- This will set "Audio output device settings" to "USB audio" and set "Change Squeezelite settings" to default values.

{{< card style="danger" >}}
"Output setting" will be blanked and will need to be set manually.
{{< /card >}}

##### Step 9 - Set Output setting

- Click [Squeezelite Settings] > "Change Squeezelite settings" > "Output setting" > "more>".
- Note the list of available CARD=E30 devices.

```text
Available output devices (click to use):

hw: devices are normally the best choice, but try and decide for yourself:

- null
- sysdefault
- default
- plugequal
- equal
- hw:CARD=E30,DEV=0
- plughw:CARD=E30,DEV=0
- sysdefault:CARD=E30
- front:CARD=E30,DEV=0
- surround21:CARD=E30,DEV=0
- surround40:CARD=E30,DEV=0
- surround41:CARD=E30,DEV=0
- surround50:CARD=E30,DEV=0
- surround51:CARD=E30,DEV=0
- surround71:CARD=E30,DEV=0
- iec958:CARD=E30,DEV=0
- hw:CARD=Headphones,DEV=0
- plughw:CARD=Headphones,DEV=0
- sysdefault:CARD=Headphones
```

- Click on "hw:CARD=E30,DEV=0".
- This will set "Output setting" to "hw:CARD=E30,DEV=0".
- Click [Save].

##### Step 10 - Reboot

- Click Reboot when requested.

##### Step 11 - Play music

- Select track/playlist.
- Play music.

## Optional

##### Step 12 - Set player name

- Access pCP using IP address in a browser (ie. http://192.168.1.xxx).
- Select [Squeezelite Settings] > "Change Squeezelite settings" > "Name of your player".
- Type player name `pCP Topping E30`.
- Click [Save].

##### Step 13 - Set hostname

- Select [Tweaks] > "pCP System Tweaks" > "Host name".
- Type host name `pCPToppingE30`.
- Click [Save].
- Click [Yes] when requested to "Reboot piCorePlayer".


## Notes

- I noticed a couple of "power up" clicks. Not a problem.


## Dignostics

- Check that the Topping E30 USB DAC has been recognised in dmesg. 
- Select  [Main Page] > [Dignostics] > [Logs] > "dmesg".
- Click [Show].
- Search for `Topping`.

```text
[    1.661609] usb 1-1.3: new high-speed USB device number 3 using xhci_hcd
[    1.792056] usb 1-1.3: New USB device found, idVendor=152a, idProduct=8750, bcdDevice= 1.08
[    1.792063] usb 1-1.3: New USB device strings: Mfr=1, Product=3, SerialNumber=0
[    1.792070] usb 1-1.3: Product: E30
[    1.792076] usb 1-1.3: Manufacturer: Topping
```

- Check that the "CARD=E30" devices are available.
- Click [Squeezelite Settings] > "Change Squeezelite settings" > "Output setting" > "more>".
- Search for `CARD=E30`.

```text
Available output devices (click to use):

hw: devices are normally the best choice, but try and decide for yourself:

- null
- sysdefault
- default
- plugequal
- equal
- hw:CARD=E30,DEV=0
- plughw:CARD=E30,DEV=0
- sysdefault:CARD=E30
- front:CARD=E30,DEV=0
- surround21:CARD=E30,DEV=0
- surround40:CARD=E30,DEV=0
- surround41:CARD=E30,DEV=0
- surround50:CARD=E30,DEV=0
- surround51:CARD=E30,DEV=0
- surround71:CARD=E30,DEV=0
- iec958:CARD=E30,DEV=0
- hw:CARD=Headphones,DEV=0
- plughw:CARD=Headphones,DEV=0
- sysdefault:CARD=Headphones
```


## More information

- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Setup Wifi without ethernet](/how-to/setup_wifi_on_pcp_without_ethernet/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
- [Topping E30 - homepage](http://www.topping-audio.com/productinfo/434825.html)
- [Topping E30 - manual](http://www.tphifi.net/drivers/e30_manual.zip)
- [LMS could not re-establish any USB connection after USB Dac is disconnected](https://forums.slimdevices.com/showthread.php?109934-LMS-could-not-reestablish-any-USB-connection-after-USB-Dac-is-disconnected/page2)
- [Possible issue with Topping E30 DAC](https://github.com/ralph-irving/squeezelite/issues/114)
