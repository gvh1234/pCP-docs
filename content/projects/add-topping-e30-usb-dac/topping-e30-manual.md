---
title: Topping E30 Manaul
description:
date: 2021-01-21
author: pCP Team
weight: 1
pcpver: "7.0.0"
toc: true
draft: false
categories:
- Projects
tags:
- USB
- DAC
---


{{< lead >}}
Thank you for purchasing Topping DAC E30!
<br />
<br />
E30 is a high performance DAC with USB, coaxial and optical inputs. It is compatible with up to 768kHz/32bit and DSD512. E30 is designed for full digital volume control and equipped with LED display screen. It can be used as not only a DAC but also preamplifier. We believe that high-quality E30 could bring you more fun at enjoying HIFI music.
{{< /lead >}}

## Contents list

 &nbsp;        | &nbsp;
---------------|-------
E30            | x1
DC cable       | x1
USB cable      | x1
User's manual  | x1
Warranty card  | x1
Remote control | x1

{{< card style="info" >}}
You can download the drivers and user's manual on http://www.tpdz.net/.
{{< /card >}}

## Attribute

 &nbsp;         | &nbsp;
----------------|--------------------------
Measured        |10.0cm x 12.5cm x 3.2cm
Weight          | 265g
Power input     | DC5V/1A
Colour          |Black/SilverlRed/Blue/Gray
Signal input    | USB/OPT/COAX
Line Out output | RCA

## Front panel

 &nbsp; | &nbsp;
--------|---------------------------
1-1     | LED screen
1-2     | Remote control receiver
1-3     | Multifunction touch button

## Rear panel

 &nbsp; | &nbsp;
--------|---------------------------
2-1     | R channel of RCA output
2-2     | L channel of RCA output
2-3     | COAX input
2-4     | OPT input
2-5     | USB input
2-6     | Power input

## Display

 &nbsp; | &nbsp;
--------|---------------------------
3-1     | Signal input indication
3-2     | Sample rate indication (in DAC mode) or volume indication (in pre-amplifier mode)
3-3     | PCM/DSD format indication

## Remote control

 &nbsp; | &nbsp;
--------|---------------------------
4-1     | Standby
4-2     | Volume up
4-3     | Switch to previous input
4-4     | Volume down
4-5     | Invalid button
4-6     | Filter setting
4-7     | Auto power on & off
4-8     | Mute
4-9     | Invalid button
4-10    | Switch to next input
4-11    | Invalid button
4-12    | Invalid button
4-13    | Brightness

## Support specification

 &nbsp;      | &nbsp;
-------------|---------------------------
USB IN:      | 44.1kHz-768kHz/16bit-32bit,
DSD64-DSD512 | (Native), DSD64-DSD256 (DoP)
OPT/COAXIN:  | 44.1kHz-192kHz/16bit-24bit

 &nbsp;         | &nbsp;
----------------|---------------------------
Output Voltage  | 2Vrms@0dBFS
THD+N <0.0003%  | @1kHz A-weighting
Noise <2uVrms   | @A-weighting
Crosstalk       |-130dB @1kHz
SNR >121dB      | @1kHZ
Dynamic Range   | 119dB @1kHz
Channel Balance | <0.3 dB

## Operation

### Power on & off/standby operation

1) Power on:

    When connects to power supply, E30 goes into the standby state, the screen displays a bright dot.

2) Standby setting:

    When it is working, long press the multifunction touch button on the front panel to enter standby state, short press to exit standby state when it is in standby. Or you can directly press the standby button by remote control to enter or exit standby state.

{{< card style="Note" >}}
When the automatic standby function is on, if the current input is not connected or input signal is invalid in 1 minute, it will automatically enter the standby state. Once a valid signal is detected on any input, it can automatically resume normal working status.
{{< /card >}}

### Volume setting

1) The enter and exit of mute state:
    Press the mute button on the remote control to set mute, re-press the mute button or adjust the volume to exit mute state.

2) Volume adjusting:
    Press the volume up and down buttons on the remote to adjust the volume of the E30.

{{< card style="Note" >}}
Volume is fixed to 0dB in DAC mode and volume adjusting is invalid in this mode.
{{< /card >}}

### Switch the input signal

Press the multifunction touch button on the front panel or press the Switch to previous input button and the Switch to next input button on the remote control to switch the input in cycle.

{{< card style="Note" >}}
"Err" will be displayed when the current input is not connected successfully.
{{< /card >}}

### Output mode setting

In the standby state, long press the multifunction touch button for 3 seconds to display the current output mode. Hold pressing, it will switch to another output mode after 1 second. The pre-amplifier mode displays “Pre” and the DAC mode displays “DAC”. When you choose the desired mode, let go and the E30 will automatically save the settings and return to standby.

### Other settings

#### 1 - PCM Filter setting

Screen display&nbsp;&nbsp;&nbsp; | Instruction
---------------|--------------------------------------------
F-1            | SHARP_ROLL_OFF_FILTER
F-2            | SLOW_ROLL_OFF_FILTER
F-3            | SHORT_DELAY_SHARP_ROLL_OFF_FILTER (Default)
F-4            | SHORT_DELAY_SLOW_ROLL_OFF_FILTER
F-5            | SUPER_SLOW_ROLL_OFF_FILTER
F-6            | LOW_DISPERSION_SHORT_DELAY_FILTER

#### 2 - DSD Filter setting

Screen display | Instruction
---------------|--------------------------------------------
F-1            | 39kHz (Default)
F-2            | 76kHZ

#### 3 - Screen brightness setting

Screen display | Instruction
---------------|--------------------------------------------
L-1            | Low
L-2            | Mid (Default)
L-3            | High

#### 4 - Auto power on & off setting

Screen display | Instruction
---------------|--------------------------------------------
A-O            | On (Default)
A-C            | Off


#### Tips

1. Other settings can be directly set by pressing the corresponding buttons on the remote control.
2. In the USB input state, press the mute button on remote control first, then press the standby button. After that, the screen will light up, you can restore the factory settings.



??? senses/seem 34  


## More information


