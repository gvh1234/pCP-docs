---
title: Autostart Squeezelite
description:
date: 2021-01-15
author: PaoloFazari
weight: 120
pCPver: "7.0.0"
toc: true
draft: false
categories:
- Projects
tags:
- USB
- DAC
---


## How to auto-start Squeezelite when power-on your USB DAC

If you have an USB DAC (or an amplifier with integrated DAC) correctly configured in pCP (by setting in squeezelite settings: USB AUDIO as audio output device and hw:CARD=xxx into output setting section - where xxx is the Linux device name of your DAC which you can see by expanding the same output section - ) you can get the problem described below.

If your DAC is turn-off, pCP (or more generally the Linux operating system) can't see the hw:card=xxx because is off. So, if pCP gets restart, squeezelite can't start. You have to intervene manually by first turning on your DAC and then going to the web page of your PC and clicking on the [Restart] squeezelite button.

The solution that I have created autostarts squeezelite (if it is down) when the DAC is turning on by checking the udev event.

This are the steps (you need "nano editor", if you don't already have it you can easily install as extension into pCP web interface):

##### Step 1

- Login into SSH shell---see [Access piCoreplayer via ssh](/how-to/access_pcp_via_ssh/).
- Turn off your DAC and then it turn on.
- Type `dmesg` and find the string "idVendor" and "idProduct" of your DAC (they are two strings of four characters).

##### Step 2

Type `sudo nano /etc/udev/rules.d/10-DAC.rules` and paste this string:

{{< code >}}
SUBSYSTEM=="usb", ACTION=="add", ATTRS{idVendor}=="xxxx", ATTR{idProduct}=="yyyy", RUN+="/home/tc/DAC.sh"
{{< /code >}}

where xxxx and yyyy are what you found in the previous step.

Press `control o` to save and `control x` to exit.

##### Step 3

Type `sudo nano /home/tc/DAC.sh` and paste this text:

{{< code lang="shell" >}}
#!/bin/sh

giar=$(sudo /usr/local/etc/init.d/squeezelite status)
if [ "$giar" = "Squeezelite not running." ];
then
	sudo /usr/local/etc/init.d/squeezelite restart
fi
{{< /code >}}

##### Step 4

Type `sudo chmod 755 /home/tc/DAC.sh` to make the script executable.

##### Step 5

Type `sudo nano /opt/.filetool.lst` and add at the end of file this string to backup the mod:

{{< code >}}
etc/udev/rules.d/10-DAC.rules
{{< /code >}}

##### Step 6

Type `pcp br` to backup and reboot.

Now, if your DAC is turn-off and squeezelite is down, when you turning on the DAC squeezelite get start.

**Credits: my wife for the patience...**

## More information

- [Development thread](https://forums.slimdevices.com/showthread.php?113661-Start-restart-squeezelite-when-plug-in-USB-dac)
- [Documentation thread](https://forums.slimdevices.com/showthread.php?112996-piCorePlayer-Documentation&p=1004445&viewfull=1#post1004445)
