---
title: Tivoli Squeezebox Radio
description:
date: 2020-08-30
author: pCP Team
weight: 50
pcpver: "6.1.0"
toc: true
draft: false
categories:
- Projects
---

{{% lead %}}
A Squeezebox built from a Tivoli Audio alarm speaker.
{{% /lead %}}

Features:

- Tivoli Speaker and case using the original speaker and 3W output.
- WiFi.
- runs on battery for a whole day (more than 12h).
- rotary and push controller for volume and play/pause.
- separate alarm clock.
- fully Squeezebox compatible player using a Raspberry Pi Zero and SqueezeLite.
- very simple and power-efficient 5V design.


## More information

- [Tivoli Squeezebox Radio](http://penguinlovesmusic.de/the-tivoli-squeezebox-radio/)
- [Building the Tivoli Squeezebox Radio](https://forums.slimdevices.com/showthread.php?107031-Building-the-Tivoli-Squeezebox-Radio)