---
title: pCP + Waveshare + jivelite
description: piCorePlayer + Waveshare 3.5" TFT + jivelite
date: 2020-09-23
author: nowhinjing
weight: 60
pcpver: "6.1.0"
toc: true
draft: false
categories:
- Projects
---

{{< lead >}}
As promised I (nowhinjing) have written up the torturous process of getting piCorePlayer + Jivelite to work on a small TFT screen.
{{< /lead >}}

## More information

- [piCorePlayer + Waveshare 3.5\" TFT + jivelite](http://www.pughx2.com/picore61.html)
- [Develpoment thread](http://forums.slimdevices.com/showthread.php?107366-picoreplayer-3-11-waveshare-3-5-TFT-jivelite-Raspberry-Pi-2B)
