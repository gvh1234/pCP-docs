---
title: Setup RemotePi Board
description: Setting up the RemotePi Board
date: 2021-01-30
author: jeroen2
weight: 130
pCPver: "7.0.0"
toc: true
draft: false
categories:
- Projects
tags:
- RemotePi
---

{{< lead >}}
The RemotePi boards offers a shutdown button and infrared for connecting a remote (when running pCP as an LMS server, doing a proper shutdown is recommended). The shutdown button is also operated by the remote so you can turn the RPi on either with the button or using the remote.
<br />
<br />
To install on pCP requires some custom scripts to be installed. Please note that to install on top of a DAC you might need to change the pin for the remote. (This can be done on the [board](https://www.msldigital.com/pages/support-for-remotepi-board-plus-2015) or in the wiring when you connect the board - I did the latter using jumper wires).
{{< /lead >}}

![RemotePi Board](RemotePiBoard4extTop_grande.jpg)

{{< card style="info" >}}
- As a starting point for this guide I assume you have pCP running and know how to access it through `ssh`---see [Access piCoreplayer via ssh](/how-to/access_pcp_via_ssh/).
- I also recommend installing Nano as an editor. (You can remove it again once everything works).
{{< /card >}}


##### Step 1a - Installing Nano

- Go to piCorePlayer [Main Page] and click on [Extensions].
- If all checks are green, click on the [Available] tab.
- From available extensions, select "nano.tcz" and click [Load].

##### Step 1b - Add the following code to disable line wrap and autoindent, which is easier when copy/pasting code

- Log in with `ssh` and type `nano .nanorc`

{{< code >}}
unset autoindent
set nowrap
{{< /code >}}

- Exit nano (ctrl-x) and save the file.

##### Step 2 - Programme your Remote on the board for power on and off

- Press the on/off button on the RemotePi board for 10 seconds until you see the green light flashing.
- Press the power key on the remote once to learn power on, and a second time to learn power off.

{{< card style="info" >}}
This step is also described as [step 3](https://www.msldigital.com/pages/support-for-remotepi-board-plus-2015) in the guide here.
{{< /card >}}

##### Step 3 - Create the shutdown monitoring and shutdown script

- Log in with `ssh` and type `sudo nano /opt/shutdown.sh`
(the code can be inserted under 'user shutdown commands', but should also work fine just replacing what's there)
{{< code lang="shell" >}}
#!/bin/sh

#send shutdown sequence to PiRemote powerboard
echo "15" > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio15/direction
echo "1" > /sys/class/gpio/gpio15/value
sleep 0.125
echo "0" > /sys/class/gpio/gpio15/value
sleep 0.2
echo "1" > /sys/class/gpio/gpio15/value
sleep 0.4
echo "0" > /sys/class/gpio/gpio15/value
echo "14" > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio14/direction
echo "1" > /sys/class/gpio/gpio14/value
sleep 6
{{< /code >}}

- Exit nano (ctrl-x) and save the file.
- Make the file executable: `sudo chmod +x /opt/shutdown.sh`


##### Step 4 - Create the monitoring script

- Log in with `ssh` and type `nano startup.sh` (this creates a file in the default location (/home/tc)

{{< code lang="shell" >}}
#!/bin/sh

#(optional) start irexec - only if you use the ir remote and want to use a custom lircrc file
#/usr/local/bin/irexec /home/tc/.lircrc &

# this is the GPIO pin receiving the shut-down signal
echo "14" > /sys/class/gpio/export
echo "in" > /sys/class/gpio/gpio14/direction

# going into a loop until shutdown
while true; do
  sleep 1
  power=$(cat /sys/class/gpio/gpio14/value)
  if [ $power != 0 ]; then
    sleep 2
    echo "out" > /sys/class/gpio/gpio14/direction
    echo "1" > /sys/class/gpio/gpio14/value
    sleep 3
    exitcheck.sh shutdown
  fi
done
{{< /code >}}

- Exit nano (ctrl-x) and save the file.
- Make the file executable: `sudo chmod +x startup.sh`

At this point you're done installing scripts. Since you're not doing this in the pCP web interface you need to backup your changes manually.
- Log in with `ssh` and type `pcp bu`

##### Step 5 - Install lirc and upload a .lircd file

- Go to the pCP web interface and click on the [Tweaks] tab.
- Go to the [IR page] and click on [Install].
- Under device settings, choose IR GPIO: 18, and IR Device: lirc0 then click [Save].

{{< card style="warning" >}}
If you changed it in the wiring, this is GPIO 17.
{{< /card >}}

You probably also need to upload a `lircd.conf` file. It depends on your remote what you need to upload here. If you bought a remote together with your RemotePi board, the contents of the contents of `lircd.conf` file can be found [here](https://www.msldigital.com/pages/support-for-remote). (look for contents of `lircd.conf` under Volumio or OpenElec until 7.0.3)

{{< card style="danger" >}}
Please note that in every configuration file you should remove the line that defines the power key, since this is processed at the RemotePi board, so pCP doesn't have to do anything with this.
{{< /card >}}

{{< card style="info" >}}You can review the various buttons on the remote and figure out what you want to use. In principle pCP has enabled a default remote key mapping for most functions on Squeezelite (pause/play/volume,etc..) so that should work out of the box, but if you want to use additional keys you also need to upload a `.lircrc` file. (I did this to use the number keys for presets.)
If you want to have pCP control the volume directly on Squeezelite, the up and down keys should be renamed to KEY_VOLUMEUP and KEY_VOLUMEDOWN.
{{< /card >}}

##### Step 6 - Run the startup script that monitors the RemotePi button

- Go to the pCP web interface and click on the [Tweaks] tab.
- Scroll down and under "User Commands" enter: `/home/tc/startup.sh`

This will make the startup script, which monitors the power button, run by default when you start pCP. You can later check the log files under [Diagnostics] to see if it does.

##### Step 7 - Shutdown and restart

You're now all set.
- Go to the [Main Page] and choose [Backup] and [Shutdown] to turn off your RPi.
- Unplug the power and plug it back in.

When you plug back in the RemotePi switch lights up red. You can now use the remote to turn on the RPi.
During startup the green LED will flash for 75 seconds. During this time your RPi is starting up (with pCP it usually takes about 30 seconds) but the green light will continue flashing regardless.

{{< card style="info" >}}
You can change the time it flashes green following the instructions on the [RemotePi website](http://www.msldigital.com/pages/configuration-tool).
{{< /card >}}

When you shutdown the RPi using the remote or the button on the RemotePi, the red light will flash until the RPi has fully shutdown and then switch off the power.

If it doesn't shutdown properly the RemotePi will continue flashing red for 4 minutes before doing a hard shutdown. If all works well though the red LED will flash for about 30 seconds until your RPi is shutdown. The RemotePi board then cuts the power and the red light inside the power button turns back on.

##### Step 8 - [Bonus] - Reduce the power on time and the LED brightness

- You can use the instructions on the [RemotePi website](http://www.msldigital.com/pages/configuration-tool) to reduce the green blinking time at startup (I set mine to 25 seconds, which is more then enough) and reduce the brightness of the power button and the LED.


## More information

- [Development thread](https://forums.slimdevices.com/showthread.php?113693-Using-the-RemotePi-board-with-pCP&p=1004738#post1004738)
- [Original thread](https://forums.slimdevices.com/showthread.php?109270-PiCore-Player-and-gpio-poweroff-gpio-shutdown-overlays&p=1004528&viewfull=1#post1004528)
- [RemotePi](https://www.msldigital.com/collections/all-products)
