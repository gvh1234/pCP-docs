---
title: Build LMS server
description: Build a simple LMS server
date: 2020-11-30
author: pCP Team
weight: 80
pcpver: "7.0.0-b6"
toc: true
draft: false
categories:
- Projects
tags:
- LMS
---

{{< lead >}}
This project is to create a simple LMS server with the library on an attached USB HDD. This is the LMS server I use day to day and it is also for pCP testing.
{{< /lead >}}


## What was used


### Hardware

- Raspberry Pi 4B - 2GB
- Official RPF 5.1V 3A PSU
- Toshiba USB 1.5TB Hard Disk Drive (HDD)
- SanDisk Ultra 8GB SD card

### Software

- piCorePlayer 7.0.0-b6 (piCorePlayer7.0.0b6-64Bit.img)
	- LMS

### Network Diagram

<div class="mermaid">
	graph TD
		B[8-port Network Switch]
		B --> |Ethernet cable| D[RPi4B - pCP LMS]
		D --- |USB cable| H[(USB HDD)]
</div>

## Steps

##### Step 1 - Download pCP

- Download piCorePlayer---see [Download piCorePlayer](/how-to/download_picoreplayer/).

##### Step 2 - Create SD card

- Create an SD card with piCorePlayer image---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).
- Eject the SD card.

##### Step 3 - Boot pCP

- Insert the SD card into the Raspberry Pi.
- Connect the Ethernet and power cables.
- Turn the power on.

##### Step 4 - Determine the IP address

- Determine the IP address---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).

##### Step 5 - Set static IP

- Set static IP on the DHCP sever (ie. http://192.168.1.51). Often the DHCP is on the router.

##### Step 6 - Set player name

- Access pCP using static IP in a browser (ie. http://192.168.1.51)
- Select [Squeezelite Settings] > "Change Squeezelite settings" > "Name of your player".
- Type player name.
- Click [Save].

##### Step 7 - Set hostname

- Select [Tweaks] > "pCP System Tweaks" > "Host name".
- Type host name.
- Click [Save].
- Click [Yes] when requested to "Reboot piCorePlayer".

##### Step 8 - Resize filesystem

- Select [Main Page] > [Beta].
- Click "Advanced mode operations" > [Resize FS].
- Select "Whole SD card"
- Click [Resize].

pCP will reboot a couple of times, but after a couple of minutes it should refresh the [Main Page].

##### Step 9 - Attach USB hard drive

- Select [LMS].
- Check what FS Type is displaying in "Pick from the following detected USB disks to mount".
- If you are using a non-default disk format, you will be prompted to install support of additional files systems.
    - Click "Install and Enable additional File Systems" > [Install].
- Type mount point name (ie. /mnt/Music).
- Tick "Enabled" checkbox.
- Click [Set USB Mount].

##### Step 10 - Install LMS

- Select [LMS].
- Click [Install].

By default LMS is not automatically started.

- Select [Start] item in "Logitech Media Server (LMS) operations".

##### Step 11 - Start LMS

- Click [Start LMS].

##### Step 12 - Configure LMS

- Click [Configure LMS].
- Set "Media Folders".

A LMS scan for music will start.

This will take some minutes.


## More information

- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
