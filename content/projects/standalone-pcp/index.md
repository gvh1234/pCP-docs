---
title: Standalone pCP
description: Standalone piCorePlayer
date: 2020-11-29
author: pCP Team
weight: 90
pcpver: "6.1.0"
toc: true
draft: false
categories:
- Projects
---

{{< lead >}}
This project outlines how to build a basic "standalone" piCorePlayer. As it is truly standalone, there is no method to control Squeezelite once the build is complete and disconnected from the network. The only method of playing music is via the LMS auto start command "randomplay tracks". There is not even a way to start/stop tracks or adjust the volume, pretty useless but it shows the basic starting point.
{{< /lead >}}
{{< lead >}}
The **next step** will be to add some form of controller. This could be via a touch screen with Jivelite, an IR remote, a Wifi Access Point or hardware control via GPIOs. Of course there is the option to leave it connected to the network via an Ethernet cable or Wifi, but be cautious of the implications of having multiple LMS servers on the same network.
{{< /lead >}}


## What was used


### Hardware
- Raspberry Pi 1B
- 8GB SD card
- USB drive with music files

### Software
- piCorePlayer 6.1.0
	- LMS
	- Static IP
- Connection to the internet


## Steps

##### Step 1 - Download pCP

- Download piCorePlayer---see [Download piCorePlayer](/how-to/download_picoreplayer/).

##### Step 2 - Prepare SD card

- Create an SD card with piCorePlayer image---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).
- Eject the SD card.

##### Step 3 - Power RPi

- Insert the SD card into the Raspberry Pi.
- Connect the Ethernet and power cables.
- Turn the power on.

##### Step 4 - Determine IP address

- Determine the IP address---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).

##### Step 5 - Access GUI

- Access the piCorePlayer's GUI from a web browser using the IP address from previous step.

##### Step 6 - Resize partition

- Select [Main page] > "Additional functions" > [Resize FS]
- Select "Resize partition 2 (PCP_ROOT)" > Select new partition size (currently 64 MB).
- Select "1000 MB".
- Click [Resize] button.

##### Step 7 - Beta mode

- Select [Beta] mode in the bottom left corner.

Check if necessary.

##### Step 8 - Install LMS

- Click [LMS] > "Logitech Media Server (LMS) operations" > [Install LMS].

##### Step 9 - Start LMS

- Click [Start LMS].

##### Step 10 - Connect USB drive

- Connect prepared USB drive.

##### Step 11 - Mount the USB drive.

- In the [LMS] > "Pick from the following detected USB disks to mount" section.
	- Click [_] enable.
	- Type mount point name (ie. /mnt/Music).
	- Click [Set USB Mount].

##### Step 12 - Configure LMS

- Click [Configure LMS].
- Select [Basic settings] > "Media folders" >  "/mnt/Music/CD".

##### Step 13 - Rescan media library

- Click "Rescan Media Library" > "Look for new and changed media files" > [Rescan].
- Click "Scanning - View Progress".

##### Step 14 - Connect Squeezelite to local server

- Select [Sqeezelite Settings] > "Change Squeezelite settings" > "LMS IP".
- Type "127.0.0.1".
- Click [Save].

##### Step 15 - Testing

{{< card style="info" >}}
This step is included for testing purposes. As soon as piCorePlayer boots some random music will play out of Headphone socket. Remove this once everything is working and you have determined how you are going to control Squeezelite.
{{< /card >}}

- Select [Tweaks] > "Auto start tweaks" > "Auto start LMS".
- Type "randomplay tracks".
- Click "Enabled".
- Click [Save].
- Click [Test].

Music plays!!!

##### Step 16 - Reboot

- Click [Main Page] > [Reboot].

Music plays!!!

##### Step 17 - Static IP

{{< card style="warning" >}}
This step is important. By setting a local static IP address, some of the network startup checking is safely bypassed.
{{< /card >}}

- Click [Main Page] > "Beta functions" > [Static IP].

##### Step 16 - Reboot

- Click [Main Page] > [Reboot].
- Remove the Ethernet cable when the Ethernet LEDs go out.

Music plays without ethernet cable!!!


## More information

- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
