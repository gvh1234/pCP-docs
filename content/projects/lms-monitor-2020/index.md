---
title: LMS Monitor
description: LMS Monitor 2020
date: 2020-03-08
author: shunte88
weight: 10
pcpver: "6.0.0"
toc: true
draft: true
categories:
- Projects
tags:
- Display
---

{{< lead >}}
OLED information display control program for piCorePlayer or other Raspberry Pi and Logitech Media Player (LMS) based audio device. Thanks shunte88. 
{{< /lead >}}

![LMS Monitor 2020](lms-monitor.jpg)

## More information

- [Announce: LMS Monitor 2020 - Forum](https://forums.slimdevices.com/showthread.php?111790-ANNOUNCE-LMS-Monitor-2020)
- [LMS Monitor 2020 - GitHub](https://github.com/shunte88/LMSMonitor)
