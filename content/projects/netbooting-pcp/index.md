---
title: Netbooting pCP
description: Netbooting piCorePlayer 3.x
date: 2020-08-30
author: pCP Team
weight: 20
pcpver: "6.1.0"
toc: true
draft: false
categories:
- Projects
---

{{< lead >}}
A seemingly operational pCP player can be obtained via net booting, but only for wired Raspberry Pi 3B's.
{{< /lead >}}

## More information

- [Netbooting piCorePlayer 3.x](http://forums.slimdevices.com/showthread.php?107233-Netbooting-piCorePlayer-3-x)
