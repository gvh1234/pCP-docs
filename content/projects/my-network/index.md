---
title: My network
description:
date: 2020-11-30
author: pCP Team
weight: 900
pcpver: "7.0.0"
toc: true
draft: true
categories:
- Projects
tags:
- Mermaid
---

{{< lead >}}
My network.
{{< /lead >}}

## Network Layout

<div class="mermaid">
	graph TD
		I[Internet] --> WR([ Wifi/Router ])
		WR --> X[Switch]
		X --> PC[Windows 10]
		PC --- |USB| G[(USB HDD)]
		X --> P1[pCP Player 1]
		P1 --- |USB| P1H[(USB HDD)]
		X --> D[pCP LMS]
		D --- |USB| H[(USB HDD)]		
		P1 --- P1S[Touch Screen]
		WR ---> |WiFi| P2H[pCP Player 2]
		WR ---> |WiFi| P3[pCP Player 3]
		P3 --- |HDMI| TV[TV Screen]
		WR ---> |WiFi| P4[pCP Player 4]
		P4 --- |HDMI| TV[TV Screen]
</div>


## Mermaid markup

```text
<div class="mermaid">
	graph TD
		I[Internet] --> WR([ Wifi/Router ])
		WR --> X[Switch]
		X --> PC[Windows 10]
		PC --- |USB| G[(USB HDD)]
		X --> P1[pCP Player 1]
		P1 --- |USB| P1H[(USB HDD)]
		X --> D[pCP LMS]
		D --- |USB| H[(USB HDD)]		
		P1 --- P1S[Touch Screen]
		WR ---> |WiFi| P2H[pCP Player 2]
		WR ---> |WiFi| P3[pCP Player 3]
		P3 --- |HDMI| TV[TV Screen]
		WR ---> |WiFi| P4[pCP Player 4]
		P4 --- |HDMI| TV[TV Screen]
</div>
```

<div class="mermaid">
	graph TB
		F[Gatsby]
		F --> G
		F --> Github
		subgraph build
			S[(Sanity)]
			G{GraphQL}
			GH[(Github)]
			G --> GH
			G --> S
		end
		subgraph deploy
			Github-->Netlify
			Netlify-->Y{{You}}
		end
</div>


<div class="mermaid">
	sequenceDiagram
		participant Client
		participant Auth server
		Client->>Auth server: Client credentials
		Auth server->>Auth server: Authenticate client
		Auth server->>Client: Access token
		Client->>API: Access protected API with access token
		API->>Client: Success
</div>

<div class="mermaid">
	journey
		title My working day
		section Go to work
		Make tea: 5: Me
		Go upstairs: 3: Me
		Do work: 1: Me, Cat
		section Go home
		Go downstairs: 5: Me
		Sit down: 3: Me
</div>

<div class="mermaid">
	pie
	"Dogs" : 386
	"Cats" : 85
	"Rats" : 15
</div>



## More information

- [Mermaid](https://mermaid-js.github.io/mermaid/#/)
