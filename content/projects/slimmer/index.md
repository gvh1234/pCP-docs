---
title: Slimmer
description:
date: 2020-08-30
author: pCP Team
weight: 40
pcpver: "6.1.0"
toc: true
draft: false
categories:
- Projects
---

{{< lead >}}
Slimmer is a user interface controller software for Logitech Media Server. Using Slimmer and other open source software and some hardware components you can build a high quality & low-cost network audio player with a slick user interface.
{{< /lead >}}

![Now playing screen](nowplaying.jpg)
![Main menu](menumain.jpg)

## More information

- [Slimmer - GitHub](https://github.com/terba/slimmer/wiki/)
- [Announce: Slimmer - Forum](https://forums.slimdevices.com/showthread.php?105819-Announce-Slimmer&p=856854&viewfull=1#post856854)
