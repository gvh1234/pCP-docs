---
title: Add HD44780 LCD
description: Add an HD44780 Liquid Crystal Display
date: 2020-09-08
author: pCP Team
weight: 120
pcpver: "6.1.0"
toc: true
draft: true
categories:
- Project
tags:
- HD47780
- LCD
---

{{< lead >}}
Add an HD47780 LCD.
{{< /lead >}}

## What you need

- Raspberry Pi 1B
- A HD44780 LCD - 1602
- 8GB SD card
- piCorePlayer 6.1.0
- Testing with Raspberry Pi OS

Boot message
Linux-5.4.51+

Reboot message
Reloading
System...

Shutdown message:
Power off.

## Files required

- /mnt/mmcblk0p1/overlays/hd44780-lcd.dtbo
- hd44780.ko - missing

Raspberry Pi OS

- /lib/modules/4.19.97+/kernel/drivers/auxdisplay/hd44780.ko
- /lib/modules/4.19.97-v7l+/kernel/drivers/auxdisplay/hd44780.ko
- /lib/modules/4.19.97-v8+/kernel/drivers/auxdisplay/hd44780.ko
- /lib/modules/4.19.97-v7+/kernel/drivers/auxdisplay/hd44780.ko
<br><br>
- /lib/modules/5.4.51+/kernel/drivers/auxdisplay/charlcd.ko
- /lib/modules/5.4.51-v7+/kernel/drivers/auxdisplay/charlcd.ko
- /lib/modules/5.4.51-v8+/kernel/drivers/auxdisplay/charlcd.ko
- /lib/modules/5.4.51-v7l+/kernel/drivers/auxdisplay/charlcd.ko

Source

- https://github.com/torvalds/linux/tree/master/drivers/auxdisplay
- linux/drivers/auxdisplay/hd44780.c
- linux/drivers/auxdisplay/charlcd.c
- linux/drivers/auxdisplay/charlcd.h

##### Step 1

Download piCorePlayer---see [Download piCorePlayer](/how-to/download_picoreplayer/).

##### Step 2

Create an SD card with piCorePlayer image---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).

Eject the SD card.

##### Step 3

Insert the SD card into the Raspberry Pi.

Connect the Ethernet cable.

Turn the power on.

##### Step 4

Determine the IP address---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).

##### Step 5

Shutdown piCorePlayer.

Turn the power off.

##### Step 6

Wire the HD44780 LCD to the RPi GPIOs.

| LCD Pin | LCD Label | RPi GPIO | RPi Pin | Note |           |
|---------|-----------|----------|---------|------|-----------|
| 1       | VSS       |          | 6       | 0V   |           |
| 2       | VDD       |          | 2       | 5V   |           |
| 3       | VO        |          |         | POT  |           |
| 4       | RS        | 25       | 22      |      | pin_rs=25 |
| 5       | RW        |          | 6       | 0V   |           |
| 6       | E         | 24       | 18      |      | pin_en=24 |
| 7       | D0        |          |         | NC   |           |
| 8       | D1        |          |         | NC   |           |
| 9       | D2        |          |         | NC   |           |
| 10      | D3        |          |         | NC   |           |
| 11      | D4        | 23       | 16      |      | pin_d4=23 |
| 12      | D5        | 17       | 11      |      | pin_d5=17 |
| 13      | D6        | 27       | 13      |      | pin_d6=27 |
| 14      | D7        | 22       | 15      |      | pin_d7=22 |
| 15      | A         |          | 2       | 5V   |           |
| 16      | K         |          | 6       | 0V   |           |

##### Step 7

Double check the wiring is correct, especially the 0 volt and 5 volt wires.

{{< card style="danger" >}} A short on the RPi's GPIO can cause permanent damage to the RPi.{{< /card >}}

##### Step 8

Access piCorePlayer via ssh---see [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/).

Add:

`dtoverlay=hd44780-lcd,pin_d4=23,pin_d5=17,pin_d6=27,pin_d7=22` <br>
`dtparam=pin_rs=25,pin_en=24,display_height=2,display_width=16`

between the **#---Begin-Custom** and **#---End-Custom** tags in config.txt.

tc@piCorePlayer:/mnt/mmcblk0p1$ `vicfg`

The bottom part of config.txt should end up looking like the following.

{{< code >}}
#Custom Configuration Area, for config settings that are not managed by pCP.
#pCP will retain these settings during insitu-update
#---Begin-Custom-(Do not alter Begin or End Tags)-----

dtoverlay=hd44780-lcd,pin_d4=23,pin_d5=17,pin_d6=27,pin_d7=22
dtparam=pin_rs=25,pin_en=24,display_height=2,display_width=16

#---End-Custom-----------------------------------------
{{< /code >}}

BAD:
`dtoverlay=hd44780-lcd,pin_rs=25,pin_en=24,pin_d4=23,pin_d5=17,pin_d6=27,pin_d7=22,display_height=2,display_width=16`

config.txt has a maximum line length of 80

##### hd44780-lcd overlay reference

{{< code >}}
Name:   hd44780-lcd

Info:   Configures an HD44780 compatible LCD display. Uses 4 gpio pins for
        data, 2 gpio pins for enable and register select and 1 optional pin
        for enabling/disabling the backlight display.

Load:   dtoverlay=hd44780-lcd,<param>=<val>

Params: pin_d4                  GPIO pin for data pin D4 (default 6)

        pin_d5                  GPIO pin for data pin D5 (default 13)

        pin_d6                  GPIO pin for data pin D6 (default 19)

        pin_d7                  GPIO pin for data pin D7 (default 26)

        pin_en                  GPIO pin for "Enable" (default 21)

        pin_rs                  GPIO pin for "Register Select" (default 20)

        pin_bl                  Optional pin for enabling/disabling the
                                display backlight. (default disabled)

        display_height          Height of the display in characters

        display_width           Width of the display in characters
{{< /code >}}


##### Step 8

Load 

##### Step 9

Shutdown the Raspberry Pi.

$ `pcp bs`

##### Step 10

Add the screen on the Raspberry Pi.

##### Step 11

Power on


## Diagnostics

pi@raspberrypi:~ $ `lsmod | grep hd44780`
```
hd44780                16384  0
charlcd                20480  1 hd44780
```
pi@raspberrypi:~ $ `ls -al /dev/lcd`
```
crw------- 1 root root 10, 156 Sep 10 08:58 /dev/lcd
```
pi@raspberrypi:~ $ `sudo find / -name hd44780.ko -print`
```
/lib/modules/5.4.51+/kernel/drivers/auxdisplay/hd44780.ko
/lib/modules/5.4.51-v7+/kernel/drivers/auxdisplay/hd44780.ko
/lib/modules/5.4.51-v8+/kernel/drivers/auxdisplay/hd44780.ko
/lib/modules/5.4.51-v7l+/kernel/drivers/auxdisplay/hd44780.ko
```
pi@raspberrypi:~ $ `uname -a`
```
Linux raspberrypi 5.4.51+ #1333 Mon Aug 10 16:38:02 BST 2020 armv6l GNU/Linux
```
pi@raspberrypi:~ $ `ls -al /boot/overlays/hd44780-lcd.dtbo`
```
-rwxr-xr-x 1 root root 1662 May 27 12:22 /boot/overlays/hd44780-lcd.dtbo
```

| HEX    | ASCII     | ESC   | Description                                |
|--------|-----------|-------|--------------------------------------------|
| 0x08   | \<BS\>    | \b    | Go back one character and clear it         |
| 0x0A   | \<LF\>    | \n    | Flush the remainder of the current line and go to the beginning of the next line   |
| 0x0C   | \<FF\>    | \f    | Clear the display                          |
| 0x0D   | \<CR\>    | \r    | Go to the beginning of the line            |
| 0x09   | \<HT\>    | \t    | Print a space instead of the tab           |
| 0x1B   | \<ESC\>   | \E    | Escape sequence begin                      |
| 0x1B   |           | \E[H  | Move cursor to home                        |
|        |           | \E[2J | Clear the display                          |
|        |           | \E[L* | Flash back light                           |
|        |           | \E[L+ | Back light on                              |
|        |           | \E[L- | Back light off                             |
|        |           | \E[LB | Blink on                                   |
|        |           | \E[Lb | Blink off                                  |
|        |           | \E[LC | Cursor on                                  |
|        |           | \E[Lc | Cursor off                                 |
|        |           | \E[LD | Display on                                 |
|        |           | \E[Ld | Display off                                |
|        |           | \E[LF | Large font                                 |
|        |           | \E[Lf | Small font                                 |
|        |           | \E[LG | Generator : LGcxxxxx...xx; must have <c> between '0' and '7', representing the numerical ASCII code of the redefined character, and <xx...xx> a sequence of 16 hex digits representing 8 bytes for each character. Most LCDs will only use 5 lower bits of the 7 first bytes.
|        |           | \E[LI | Reinitialise display                       |
|        |           | \E[Lk | Kill end of line                           |
|        |           | \E[LL | Shift display left                         |
|        |           | \E[Ll | Shift cursor left                          |
|        |           | \E[LN | Two lines                                  |
|        |           | \E[Ln | One line                                   |
|        |           | \E[LR | Shift display right                        |
|        |           | \E[Lr | Shift cursor right                         |
|        |           | \E[Lx | Goto x : LxXXX[yYYY];                      |
|        |           | \E[Ly | Goto y : LyYYY[xXXX];                      |

```
#!/bin/bash

#--------|-----------|-------|--------------------------------------------|------------|
# HEX    | ASCII     | ESC   | Description                                | Function   |
#--------|-----------|-------|--------------------------------------------|------------|
# 0x08   | \<BS\>    | \b    | Go back one character and clear it         | back_char  |
# 0x0A   | \<LF\>    | \n    | Flush the remainder of the current line    |
#        |           |       | and go to the beginning of the next line   |
# 0x0C   | \<FF\>    | \f    | Clear the display                          | clear2
# 0x0D   | \<CR\>    | \r    | Go to the beginning of the line            |
# 0x09   | \<HT\>    | \t    | Print a space instead of the tab           |
# 0x1B   | \<ESC\>   | \E    | Escape sequence begin                      |
# 0x1B   |           | \E[H  | Move cursor to home                        |
#        |           | \E[2J | Clear the display                          |
#        |           | \E[L* | Flash back light                           |
#        |           | \E[L+ | Back light on                              |
#        |           | \E[L- | Back light off                             |
#        |           | \E[LB | Blink on                                   |
#        |           | \E[Lb | Blink off                                  |
#        |           | \E[LC | Cursor on                                  |
#        |           | \E[Lc | Cursor off                                 |
#        |           | \E[LD | Display on                                 |
#        |           | \E[Ld | Display off                                |
#        |           | \E[LF | Large font                                 |
#        |           | \E[Lf | Small font                                 |
#        |           | \E[LG | Generator : LGcxxxxx...xx; must have <c>   |
#        |           |       | between '0' and '7', representing the      |
#        |           |       | numerical ASCII code of the redefined      |
#        |           |       | character, and <xx...xx> a sequence of 16  |
#        |           |       | hex digits representing 8 bytes for each   |
#        |           |       | character. Most LCDs will only use 5 lower |
#        |           |       | bits of the 7 first bytes.                 |
#        |           | \E[LI | Reinitialise display                       |
#        |           | \E[Lk | Kill end of line                           |
#        |           | \E[LL | Shift display left                         |
#        |           | \E[Ll | Shift cursor left                          |
#        |           | \E[LN | Two lines                                  |
#        |           | \E[Ln | One line                                   |
#        |           | \E[LR | Shift display right                        |
#        |           | \E[Lr | Shift cursor right                         |
#        |           | \E[Lx | Goto x : LxXXX[yYYY];                      |
#        |           | \E[Ly | Goto y : LyYYY[xXXX];                      |
#--------|-----------|-------|--------------------------------------------|

scrn() {
	echo -e -n "$@" > /dev/lcd
}

cmd() {
	echo -e -n "\e[L$@" > /dev/lcd
}

back_char() {
	scrn "\b"
}

clear2() {
	scrn "\f"
}

newline() {
	scrn "\n"
}

begin_bol() {
	scrn "\r"
}

space() {
	scrn "\t"
}

cursor_home() {
	scrn "\E[H"
}

clear() {
	scrn "\E[2J"
}

backlight_flash() {
	cmd *
}

backlight_on() {
	cmd +
}

backlight_off() {
	cmd -
}

blink_on() {
	cmd B
}

blink_off() {
	cmd b
}

cursor_on() {
	cmd C
}

cursor_off() {
	cmd c
}

display_on() {
	cmd D
}

display_off() {
	cmd d
}

small_font() {
	cmd f
}

large_font() {
	cmd F
}

initialise() {
	cmd I
}

kill_eol() {
	cmd k
}

display_left() {
	cmd L
}

cursor_left() {
	cmd l
}

x2_lines() {
	cmd N
}

x1_line() {
	cmd n
}

display_right() {
	cmd R
}

cursor_right() {
	cmd r
}

initialise
scrn '1. LCD test'
newline
scrn '2. Cursor test'
sleep 2

display_off
sleep 2
cursor_off
blink_off
display_on

sleep 2

clear
cursor_home
scrn "ABCDEFGHIJKLMNOPQRSTUVWXYZ\n"
scrn "01234567890123456789012345"

sleep 2

display_left
sleep 1
display_left
sleep 1
display_right
sleep 1
display_right

clear
cursor_home
scrn "CURSOR POSITION\n"
scrn "TEST"

sleep 2

cursor_on
cursor_home
sleep 1
cursor_right
sleep 1
cursor_right
sleep 1
cursor_left
sleep 1
cursor_left

LOOP=0
while [ $LOOP -lt 15 ]; do
	sleep 1
	cursor_right
	LOOP=$(($LOOP + 1))
done
while [ $LOOP -gt 0 ]; do
	sleep 1
	cursor_left
	LOOP=$(($LOOP - 1))
done
```



## More information

- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
- [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/)
- [Edit config.txt](/how-to/edit_config_txt/)
- [piCorePlayer aliases](/information/pcp_aliases/)
- [piCorePlayer CLI](/information/pcp_cli/)
- [Basic vi commands](https://www.cs.colostate.edu/helpdocs/vi.html)
- [Raspberry Pi config.cfg](http://elinux.org/index.php?title=RPiconfig)
- [Hitachi HD44780 LCD controller](https://en.wikipedia.org/wiki/Hitachi_HD44780_LCD_controller)
