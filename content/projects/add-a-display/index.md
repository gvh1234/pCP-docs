---
title: Add a 3.5 inch display
description:
date: 2020-10-27
author: pCP Team
weight: 100
pcpver: "6.1.0"
toc: true
draft: false
categories:
- Project
tags:
- Display
---

{{< lead >}}
piCoreplayer by default supports the official Raspberry Pi 7" Touchscreen Display. However, there are dozens of screens available for Raspberry Pi. These instructions are for a generic 3.5inch RPi Display but may also give clues how to support other screens. A big thanks to nowhinjing for his Waveshare 4.1 TFT + piCorePlayer + Jivelite tutorial which was used as a starting point.
{{< /lead >}}

## What was used

- Raspberry Pi 3B
- A 3.5 TFT screen - 3.5inch RPi Display - 480x320 Pixel - XPT2046 Touch Controller
- 8GB SD card
- piCorePlayer 6.1.0

##### Step 1

Download piCorePlayer---see [Download piCorePlayer](/how-to/download_picoreplayer/).

##### Step 2

Create an SD card with piCorePlayer image---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).

Eject the SD card.

##### Step 3

Insert the SD card into the Raspberry Pi.

Connect the Ethernet cable.

Turn the power on.

##### Step 4

Determine the IP address---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).

##### Step 5

Access piCorePlayer via ssh---see [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/).

Edit the file cmdline.txt---see [Edit config.txt](/how-to/edit_config_txt/).

Add `fbcon=map:10 fbcon=font:ProFont6x11 logo.nologo` to the end of the first line in `cmdline.txt`.

tc@piCorePlayer:~$ `m1`

tc@piCorePlayer:~$ `c1`

tc@piCorePlayer:/mnt/mmcblk0p1$ `vicmd`

{{< card style="warning" >}}
cmdline.txt should look like the following. It must be just one long line of boot codes. Adding extra lines will cause boot failure.
{{< /card >}}

```text
tc@piCorePlayer:/mnt/mmcblk0p1$ cat cmdline.txt
tz=EST-10EST,M10.1.0,M4.1.0/3 dwc_otg.fiq_fsm_mask=0xF host=piCorePlayer dwc_otg.lpm_enable=0 console=tty1 root=/dev/ram0 elevator=deadline rootwait quiet nortc loglevel=3 noembed smsc95xx.turbo_mode=N noswap consoleblank=0 waitusb=2 fbcon=map:10 fbcon=font:ProFont6x11 logo.nologo
```

Options:
- fbcon=map:10
- fbcon=font:ProFont6x11
- logo.nologo

{{< card style="info" >}}
These 3 options may not really be required. Try removing them one at a time. Let us know.
{{< /card >}}

##### Step 6

Access piCorePlayer via ssh---see [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/).

Add `dtoverlay=piscreen,speed=24000000,rotate=90` between the lines starting with **#---Begin-Custom** and **#---End-Custom** in config.txt.

tc@piCorePlayer:/mnt/mmcblk0p1$ `vicfg`

The bottom part of config.txt should end up looking like the following.

```text
#Custom Configuration Area, for config settings that are not managed by pCP.
#pCP will retain these settings during insitu-update
#---Begin-Custom-(Do not alter Begin or End Tags)-----

dtoverlay=piscreen,speed=24000000,rotate=90

#---End-Custom-----------------------------------------
```

Options:
- rotate=90
- rotate=270

###### piscreen overlay description:

```text
Name:   piscreen
Info:   PiScreen display by OzzMaker.com
Load:   dtoverlay=piscreen,<param>=<val>
Params: speed                   Display SPI bus speed

        rotate                  Display rotation {0,90,180,270}

        fps                     Delay between frame updates

        debug                   Debug output level {0-7}

        xohms                   Touchpanel sensitivity (X-plate resistance)
```

##### Step 7

Resize the root partition.

From the [Main Page] click the [Resize FS] button and choose to increase the size to at least 100 MB.

##### Step 8

Load pcp-jivelite_default-wav35skin.tcz

The default skin and all its dependencies will then be automatically loaded.

From the [Main Page] and click the [Extensions] button at the end of the "Additional functions" section.

Select the "pcp-jivelite_default-wav35skin.tcz" extension from the piCorePlayer main repository and click the [Load] button.

This loads the following extensions.

```text
-----pcp-jivelite_default-wav35skin.tcz
     |-----pcp-jivelite_wav35skin.tcz
     |     |-----pcp-jivelite.tcz
     |     |     |-----touchscreen-4.19.122-pcpCore_v7.tcz
     |     |     |-----libts.tcz
     |     |     |-----pcp-lua.tcz
     |     |     |-----pcp-jivelite_hdskins.tcz
```

##### Step 9

Shutdown the Raspberry Pi.

$ `pcp bs`

##### Step 10

Add the screen on the Raspberry Pi.

##### Step 11

Power on

{{< card style="info" >}}
At this stage the touch screen should be working as just a console window, without Jivelite.
{{< /card >}}

##### Step 12

Create a file jivelite.sh in /mnt/mmcblk0p2/tce/

$ `ce`

$ `vi jivelite.sh`

Copy and paste the code below into jivelite.sh

{{< code >}}
#!/bin/sh

EVENTNO=$(cat /proc/bus/input/devices | awk '/ADS7846 Touchscreen/{for(a=0;a>=0;a++){getline;{if(/mouse/==1){ print $NF;exit 0;}}}}')

export JIVE_NOCURSOR=1
export SDL_VIDEODRIVER=fbcon
export SDL_FBDEV=/dev/fb1
export TSLIB_TSDEVICE=/dev/input/$EVENTNO
export SDL_MOUSEDRV=TSLIB

while true; do
    /opt/jivelite/bin/jivelite
    sleep 3
done
{{< /code >}}

Set jivelite.sh as executable.

$ `chmod 755 jivelite.sh`

{{< card style="warning" >}}
The script above is assuming the device name is ADS7846 Touchscreen. To determine device name use the command below.
{{< /card >}}


```text
tc@piCorePlayer:~$ cat /proc/bus/input/devices | grep N:
N: Name="ADS7846 Touchscreen"
tc@piCorePlayer:~$
```

##### Step 13

Calibrate the Touch Screen.

$ `sudo TSLIB_FBDEVICE=/dev/fb1 TSLIB_TSDEVICE=/dev/input/event0 /usr/local/bin/ts_calibrate`

You should see the following 2 lines on the screen.

```text
Touchscreen calibration utility
Touch crosshair to calibrate
```

Touch the 5 crosshairs, 4 corners plus center.

```text
tc@piCorePlayer:~$ sudo TSLIB_FBDEVICE=/dev/fb1 TSLIB_TSDEVICE=/dev/input/event0 /usr/local/bin/ts_calibrate
xres = 480, yres = 320
Took 10 samples...
Top left : X = 3858 Y =  342
Took 12 samples...
Top right : X =  321 Y =  344
Took 3 samples...
Bot right : X =  315 Y = 3814
Took 12 samples...
Bot left : X = 3841 Y = 3734
Took 8 samples...
Center : X = 2078 Y = 2026
512.168335 -0.130261 -0.000433
-21.502014 0.001012 0.087424
Calibration constants: 33565464 -8536 -28 -1409156 66 5729 65536
```

##### Step 14

Check that the calibration has been successful.

$ `sudo TSLIB_FBDEVICE=/dev/fb1 TSLIB_TSDEVICE=/dev/input/event0 /usr/local/bin/ts_test`

You should see the following 2 lines on the screen.

```text
Touchscreen test program
Touch screen to move crosshair
```

Move the stylus around the screen and the crosshair should follow.

Touch [Quit] button on the top right to exit test.

```text
tc@piCorePlayer:~$ sudo TSLIB_FBDEVICE=/dev/fb1 TSLIB_TSDEVICE=/dev/input/event0 /usr/local/bin/ts_test
1599049890.101942:    246    164      6
1599049890.121943:    246    164     19
1599049890.142008:    246    164     28
1599049890.161937:    246    164     31
1599049890.181949:    246    165     37
1599049890.207568:    247    165     40
1599049890.221956:    248    165     42
1599049890.242051:    249    165     43
1599049890.261960:    251    165     43
1599049890.281957:    252    165     42
1599049890.304261:    253    165     43
1599049890.321945:    255    165     42
tc@piCorePlayer:~$
```

##### Step 15

Go to the [Tweaks] page and set "Set Autostart" to "yes". Click [Set Autostart] to save.

Click [Main Page] > [Reboot].

##### Step 16

Setup Jivelite.

Select language.

##### Step 17

Click [Main Page] > [Backup] to save Jivelite settings.


## More information

- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
- [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/)
- [Edit config.txt](/how-to/edit_config_txt/)
- [piCorePlayer aliases](/information/pcp_aliases/)
- [piCorePlayer CLI](/information/pcp_cli/)
- [Basic vi commands](https://www.cs.colostate.edu/helpdocs/vi.html)
- [Raspberry Pi config.cfg](http://elinux.org/index.php?title=RPiconfig)
- [Waveshare 4.1 TFT + piCorePlayer + jivelite](/projects/waveshare-tft-pcp-jivelite/)
    - [Develpoment thread](http://forums.slimdevices.com/showthread.php?107366-picoreplayer-3-11-waveshare-3-5-TFT-jivelite-Raspberry-Pi-2B)
