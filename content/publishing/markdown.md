---
title: Markdown
description: Markdown syntax guide
date: 2020-10-26
author: pCP Team
weight: 20
pcpver: "6.1.0"
toc: true
draft: false
categories:
- Publishing
tags:
- Markdown
---


This page contains examples of Markdown syntax. For more detailed descriptions of Markdown syntax---see:

- [CommonMark help](http://commonmark.org/help/) 
- [CommonMark specification](http://spec.commonmark.org/).
- [Learn Markdown](https://gohugo.io/content-management/formats/#learn-markdown)
- [Markdown Guide](https://www.markdownguide.org/)


## Headings

The following HTML `<h1>`—`<h6>` elements represent six levels of section headings.

###### Example:

```text
# This is an H1 - Do not use - Defined by title/description front matter
## This is an H2 - added to table of contents
### This is an H3 - added to table of contents
#### This is an H4
##### This is an H5 - label
###### This is an H6
```

###### Result:

## This is an H2 - added to table of contents
### This is an H3 - added to table of contents
#### This is an H4
##### This is an H5 - label
###### This is an H6

## Paragraphs

Paragraphs are separated by empty lines. To create a new paragraph, press `<return>` twice.

```text
Paragraph 1

Paragraph 2
```

## Character styles

###### Example:

```text
*Italic characters*
_Italic characters_
**bold characters**
__bold characters__
~~strikethrough text~~
```

###### Result:

*Italic characters*<br>
_Italic characters_<br>
**bold characters**<br>
__bold characters__<br>
~~strikethrough text~~


## Unordered list

###### Example:

```text
-  Item 1
-  Item 2
-  Item 3
    -  Item 3a
    -  Item 3b
    -  Item 3c
```

###### Result:

-  Item 1
-  Item 2
-  Item 3
    -  Item 3a
    -  Item 3b
    -  Item 3c


## Ordered list

###### Example:

```text
1.  Step 1
1.  Step 2
1.  Step 3
    1.  Step 3.1
    1.  Step 3.2
    1.  Step 3.3
```

###### Result:

1.  Step 1
1.  Step 2
1.  Step 3
    1.  Step 3.1
    1.  Step 3.2
    1.  Step 3.3

## Inline code characters

    Use the backtick to refer to a `function()`.

    There is a literal ``backtick (`)`` here.

## Code blocks

###### Example:

```text
Indent every line of the block by at least 4 spaces.
This is a normal paragraph:
    This is a code block.
    With multiple lines.
```

Alternatively, you can use 3 backtick quote marks before and after the block, like this:

###### Example:

```text
    ```
    This is a code block using 3 backticks
    ```
```

###### Result:

```
This is a code block using 3 backticks
```

###### Example:

    To add syntax highlighting to a code block, add the name of the language immediately
    after the backticks: 

    ```javascript
    var oldUnload = window.onbeforeunload;
    window.onbeforeunload = function() {
        saveCoverage();
        if (oldUnload) {
            return oldUnload.apply(this, arguments);
        }
    };
    ```

###### Result:

```javascript
var oldUnload = window.onbeforeunload;
window.onbeforeunload = function() {
    saveCoverage();
    if (oldUnload) {
        return oldUnload.apply(this, arguments);
    }
};
```

## Links to external websites

###### Example:

```text
This is [an example](http://www.example.com/) inline link.

[This link](http://example.com/ "Title") has a title attribute.

Links are also auto-detected in text: http://example.com/
```

###### Result:

This is [an example](http://www.example.com/) inline link.

[This link](http://example.com/ "Title") has a title attribute.

Links are also auto-detected in text: http://example.com/


## Images

Inline image syntax looks like this:

###### Example:

```text
![Alt text](/path/to/image.jpg)
![Alt text](/path/to/image.png "Optional tool tip")
![Alt text](/url/to/image.jpg)
```


## Tables

###### Example:

```text
| Day     | Meal    | Price |
| --------|---------|-------|
| Monday  | pasta   | $6    |
| Tuesday | chicken | $8    |
```

###### Result:

| Day     | Meal    | Price |
| --------|---------|-------|
| Monday  | pasta   | $6    |
| Tuesday | chicken | $8    |


## Backslash escapes

Certain characters can be escaped with a preceding backslash to preserve the literal display of a character instead of its special Markdown meaning. This applies to the following characters:

`\  backslash`<br>
`` `  backtick``<br>
`*  asterisk`<br>
`_  underscore`<br>
`{} curly braces`<br>
`[] square brackets`<br>
`() parentheses`<br>
`#  hash mark`<br>
`>  greater than`<br>
`+  plus sign`<br>
`-  minus sign (hyphen)`<br>
`.  dot`<br>
`!  exclamation mark`


## More information

- [Learn Markdown](https://gohugo.io/content-management/formats/#learn-markdown)
- [goldmark Spec](https://github.com/yuin/goldmark/)
- [CommonMark help](http://commonmark.org/help/)
- [CommonMark specification](http://spec.commonmark.org/)

