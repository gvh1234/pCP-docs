---
title: Create new document
description: Create a new document
date: 2020-10-26
author: pCP Team
weight: 10
pcpver: "6.1.0"
toc: true
draft: false
categories:
- Publishing
---

{{< lead >}}
This is how you can create a new document for inclusion into piCorePlayer Documentation.
{{< /lead >}}

{{< lead >}}
There are various options with the intention of making it easy to submit content to the piCorePlayer Documentation. You can start by submitting a "plain text file" right up to using Markdown with shortcodes and using Hugo to publish to a local server for checking.
{{< /lead >}}


## Plain text

This is the simplest way to do it.

A user:
- Creates a plain text document using a text editor.
- Submits the document as an attachment through the piCorePlayer Documentation thread---see [piCorePlayer Documentation](https://forums.slimdevices.com/showthread.php?112996-piCorePlayer-Documentation)
	- Reply to thread
	- Attachments

The pCP Team will then:
- Apply Markdown formatting.
- Add front matter.
- Do an edit pass.
- Check on local server.
- Push to production server.


## Markdown

If you are familiar with Markdown formatting then you can help us out by doing the formatting for us. Markdown is fairly basic so it is reasonably simple to master---see [Markdown](/publishing/markdown/).

A user:
- Creates a Markdown document using a text editor.
- Submits the document as an attachment through the piCorePlayer Documentation thread---see [piCorePlayer Documentation](https://forums.slimdevices.com/showthread.php?112996-piCorePlayer-Documentation)
	- Reply to thread
	- Attachments

The pCP Team will then:
- Add front matter.
- Do an edit pass.
- Check on local server.
- Push to production server.


## Web page

If you already have a web page, know of one, or prefer using your own HTML tools to produce content, then simply give us the URL. We can scrape the HTML to create a Markdown document.

A user:
- Creates a page or blog using their normal tools.
- Submits the URL through the piCorePlayer Documentation thread---see [piCorePlayer Documentation](https://forums.slimdevices.com/showthread.php?112996-piCorePlayer-Documentation)
	- Reply to thread

The pCP Team will then:
- Scrape HTML from the web page.
- Convert HTML to Markdown.
- Add front matter.
- Do an edit pass.
- Check on local server.
- Push to production server.


## Merge request

Coming soon!

We intend to make the GitLab piCorePlayer Documentation repository public, so you if you are a git user you will be able to do "merge request".


## More information

- [piCorePlayer Documentation](https://forums.slimdevices.com/showthread.php?112996-piCorePlayer-Documentation) - Forum thread
- [Markdown](/publishing/markdown/)
- [Hugo Shortcodes](/publishing/shortcodes/)
- [Front matter](/publishing/front_matter/)
