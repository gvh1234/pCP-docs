---
title: Front matter
description: Front matter - YAML meta data
date: 2020-09-26
author: pCP Team
weight: 40
pcpver: "6.1.0"
toc: true
draft: false
categories:
- Publishing
tags:
- Front matter
---


Front matter - YAML formatted.


## Example front matter

```yaml
---
title: Front matter
description: Front matter - YAML meta data
date: 2020-09-24
author: pCP Team
weight: 6
pcpver: "6.1.0"
toc: false
draft: false
hidden: false
categories:
- Publishing
tags:
- Front matter
---
```

## Variables


### Title

The `title` variable:

- is used in the `left hand navigation menu`.
- is used in the `Previous/Next` links at the bottom of the page.
- should be relatively brief. It is desireable for the the title to be a single line in the navigation menu. For example, use pCP instead of piCorePlayer.
- often the same or very similar to the file name.
- is used as the page title if `description` is not defined. It will be rendered as a Heading 1 at the top of the page.

### Description

The `description` variable:

- is used as the page title. It will be rendered as a Heading 1 at the top of the page. If `description` is left blank, `title` will be used as the page title.
- is a longer, more descriptive title. For example, use piCorePlayer instead of pCP.

### Date

The `date` variable:

- is latest edit date of the page.
- uses YYYY-MM-DD format.
- is wrapped in double quotes.
- can stop Hugo from publishing the file if the `date` is in the future.

### Author

The `author` variable:

- is the author of the document.
- defaults to "pCP Team".

### Weight

The `weight` variable:

- controls the order of the page in the navigation menu.
- should start with a weight gap of 10 between each document to allow the insertion of documents in the future.

### pcpver

The `pcpver` variable:

- is the current version piCorePlayer at the time the document was written.
- gives the reader an indication as to how dated the information is.

### TOC

The `toc` variable:

- is set to `true` or `false`.
- is by default set to `true`.
- controls the display of a `On this page` table of contents on the right hand side of the page.

### Draft

The `draft` variable:

- is set to `true` or `false`.
- is by default set to `true`.
- controls if Hugo will not publish the document. Documents will only be published if `draft: false` is set. 

### Hidden

The `hidden` variable:

- is set to `true` or `false`.
- is by default set to `false`.
- controls the display of the document in the navigation menu on the left hand side of the page.

### Categories

The `categories` variable:

- adds the document to a `category`.

### Tags

The `tags` variable:

- adds the document to a `tag`.


## Default archetype

```text
Hugo
├───bin
├───pCP-docs
│   ├───archetypes
│   │   └───default.md
│   ├───content
```

```yaml
---
title: {{ replace .Name "-" " " | title }}
description:
date: {{ .Date }}
author: pCP Team
weight: 1
pcpver: "6.1.0"
toc: true
draft: true
hidden: false
categories:
- Cat A
- Cat B
tags:
- Tag A
- Tag B
---
```

## More information

- [Hugo front matter](https://gohugo.io/content-management/front-matter/)
