---
title: Add a new release
description:
date: 2020-11-22
author: pCP Team
weight: 1010
pcpver: "6.1.0"
toc: true
draft: false
---

{{< lead >}}
Instructions for pCP Team Only.
{{< /lead >}}


## Adding a Download

#### Step 1. - Start Hugo

$ `cd c:\Hugo\pCP-docs`

$ `hugo server`

#### Step 2. - Edit pCP-docs\content\downloads\\_index.md

##### Example of existing _index.md (Front matter - YAML formatting)

```yaml
---
title: Downloads
description:
date: 2020-08-08   <-- edit
author: pCP Team
weight: 6
pcpver: "6.1.0"    <-- edit
toc: false
draft: false

download:
- pcpversion: "6.1.0"   --
  pcpdate: 2020-06-06     |  cut and paste 4 lines
  url: false              |  indentation 2 spaces (tabs not allowed)
  realtime: true        --
- pcpversion: "6.0.0"
  pcpdate: 2020-03-07
  url: false
  realtime: true
- pcpversion: "5.0.0"
  pcpdate: 2019-05-26
  url: false
  realtime: true



  ---
```

- edit date: ---future dates wouldn't publish
- edit pcpver: 
- cut and paste 4 lines immediately below download:
- edit pcpversion:
- edit pcpdate:

##### Resulting _index.md

```yaml
---
title: Downloads
description:
date: 2020-08-18     <- updated
author: pCP Team
weight: 6
pcpver: "7.0.0"      <- updated
toc: false
draft: false

download:
- pcpversion: "7.0.0"    <- new  
  pcpdate: 2020-08-18    <- new
  url: false             <- new
  realtime: true         <- new
- pcpversion: "6.1.0"
  pcpdate: 2020-06-06
  url: false
  realtime: true
- pcpversion: "6.0.0"
  pcpdate: 2020-03-07
  url: false
  realtime: true
- pcpversion: "5.0.0"
  pcpdate: 2019-05-26
  url: false
  realtime: true



---  
```

- Hugo uses template pCP-docs\themes\ace-documentation\layouts\downloads\list.html
- Hugo will detect changes to _index.md and will republish. Browser will auto update.


## Adding a Release

#### Step 1. - Copy pCP-docs\content\releases\pcp6.1.0.md

$ `cd c:\Hugo\pCP-docs\content\releases\`

$ `cp pcp6.1.0.md pcp7.0.0.md`

#### Step 2. - Edit pCP-docs\content\releases\pcp7.0.0.md

- Update front matter - title:, date: and pcpver:
- Update content - Significant changes and Notes

```txt
---
title: piCorePlayer 6.1.0   <-- edit
description:
date: 2020-06-06            <-- edit
author: pCP Team
weight: 1
pcpver: "6.1.0"             <-- edit
toc: true
draft: false
---

The pCP Team has released an new version of piCorePlayer.

For support---see this topic on [Squeezebox Forum](https://www.picoreplayer.org/announce.6.0.0).     <-- edit

### Significant changes

- Kernel 4.19.122                       --
- RPi Firmware May 22, 2020               | Update
- Includes support for RPi4B 8GB boards.  |
- Support for Merus Amp.                --

### Notes

- Adding a file named "netusb" to the boot partition will automatically load/configure net-usb kernel modules.     <-- Update
- Adding wpa_supplicant.conf to the boot partition will automatically enable Wifi.     <-- Update

### Download

New images or insitu update are available. The released versions can be downloaded from here.

- [piCorePlayer 6.1.0 - Standard Version](https://repo.picoreplayer.org/insitu/piCorePlayer6.1.0/piCorePlayer6.1.0.zip)    <-- edit
- [piCorePlayer 6.1.0 - Experimental RealTime Kernel](https://repo.picoreplayer.org/insitu/piCorePlayer6.1.0/piCorePlayer6.1.0-Audio.zip)   <-- edit

The pCP Team<br>
Paul, Greg, Ralphy and Steen
```

## More information

- By default, Hugo will not publish files marked "draft: true".
- By default, Hugo will not publish files with future dates.
- By default, Hugo creates html files in memory.
- Depending on options, Hugo can create a \public folder. Do not push to remote. 
