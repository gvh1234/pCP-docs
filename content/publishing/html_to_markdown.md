---
title: HTML to Markdown
description: Convert HTML to Markdown
date: 2020-10-26
author: pCP Team
weight: 70
pcpver: "6.1.0"
toc: true
draft: false
categories:
- Publishing
tags:
- HTML
- Markdown
---

##### Step 1

In web page, View source (`Ctrl+U` in Edge)

Copy the HTML code by:
- clicking on HTML window to set focus
- type `ctrl+a`
- type `ctrl+c`

##### Step 2

Open Turndown---click [Turndown - HTML to Markdown](https://domchristie.github.io/turndown/)

- Set `Heading style` to `atx`
- Set `Bullet` to `-`
- Set `Code block style` to `fenced`

Paste in left HTML window `ctrl+v`

Copy result from right MARKDOWN window `ctrl+a`, `ctrl+c`

##### Step 3

Paste into text editor `ctrl+c`

Remove head and trailing rubbish


 ## More information

- [Turndown - HTML to Markdown](https://domchristie.github.io/turndown/)
