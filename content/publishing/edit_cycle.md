---
title: Edit cycle
description:
date: 2020-11-26
author: pCP Team
weight: 1000
pcpver: "6.1.0"
toc: true
draft: false
---

{{< lead >}}
These instructions are intended for pCP Team. 
{{< /lead >}}


#### Step 1 - Start Hugo

$ `cd c:\Hugo\pCP-docs`

$ `hugo server`

**Optional:**

Publish draft documents (draft: true)

$ `hugo server -D`

Publish future dated documents

$ `hugo server -F` 

Force a republish of everything

$ `hugo server --disableFastRender`


#### Step 2 - Edit/add files under c:\Hugo\pCP-docs\content
<br>

```
C:.
├───bin
└───pCP-docs
    ├───archetypes
    ├───content
    │   ├───admin
    │   ├───components
    │   ├───downloads
    │   ├───faq
    │   ├───getting-started
    │   ├───how-to
    │   ├───information
    │   ├───projects
    │   └───releases
    ├───data
    ├───layouts
    ├───resources
    ├───static
    └───themes
```

#### Step 3 - View changes

Type `http://localhost:1313/` into browser.

Hugo will have automatically detected changes and republished new content.

Browser will auto update showing changes.

#### Step 4 - Stage, commit and push to remote

As soon as the new content arrives in "master" branch a GitLab CI converts markdown to html and puts it into public folder (GitLab pages).

#### Step 5 - View in production

In browser type `https://docs.picoreplayer.org/`
