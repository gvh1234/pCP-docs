---
title: Shortcodes
description:
date: 2020-10-26
author: pCP Team
weight: 30
pcpver: "6.1.0"
toc: true
draft: false
categories:
- Publishing
tags:
- Shortcodes
---

{{< lead >}}
Shortcodes are used to add a little more functionality to Markdown. Shortcodes are located in the theme layouts directory.
{{< /lead >}}


## Card shortcode

Draw the reader's attention by separating information from the rest of the page. Convey meaning to this information by using colors implying success, info, warning, or danger and an appropriate icon.

{{< card style="danger" >}}
Be sure to read me, I might have important information for you.
{{< /card >}}

### Usage

Simply place the following shortcode in the document.

{{< code lang="html" >}}
{{</* card style="STYLE" */>}} [content goes here] {{</* /card */>}}
{{< /code >}}

The STYLE parameter is directly applied to the card as a class in the format **"border-{STYLE}"**. Bootstrap has a variety of standard styles that can be used:

- primary
- success
- info
- warning
- danger

Example: `style="danger"`.


## Childpages shortcode

The childpages shortcode allows you to show a list of sub-pages of the current page. Placing the childpages shortcode in the *How-to* page will place a create a list of these sub-pages in that page.

### Usage

Simply place the following shortcode in the document.

{{< code lang="html" >}}
{{</* childpages */>}}
{{< /code >}}


## Code shortcode

Add code to your page with syntax highlighting and a copy button so your users can easily copy the code to their clipboard with the press of a button. The code may be entered inside the shortcode or come from an external file.

{{< code lang="html" >}}
<div class="mydiv bg-primary shadow text-white">
	<h1 class="title">Hi there</h1>
	<p class="lead">I'm inside a code shortcode. Check out my syntax highlighting!.</p>
</div>
{{< /code >}}

### Usage

Simply place the following shortcode on the page

#### Code in the shortcode

{{< code lang="html" >}}
{{</* code lang="LANG" */>}} [your code] {{</* /code */>}}
{{< /code >}}

#### Code from a file

{{< code lang="html" >}}
{{</* code lang="LANG" file="code/mycode.html" */>}}
{{< /code >}}


### Parameters

#### *lang*
The lang parameter defines the language to be used for code highlighting. You can find a complete list of supported languages <a href="https://gohugo.io/content-management/syntax-highlighting/#list-of-chroma-highlighting-languages" target="_blank">here</a>.

Example: <code>lang="html"</code>

#### *file*
The file parameter allows you to define an external file that contains your code to be displayed. This is done by giving a path to that file, starting from the root directory of your site.
For example, a HTML file named *'mycode.html'* you wish to link that is in the *docs/code/* directory can be defined as follows: <code>file="code/mycode.html"</code>.


## Lead shortcode

The lead shortcode creates a paragraph with the *lead* class to make it stand out. Great for introductions or summaries.

{{< lead >}}
I'm a lead paragraph. That means I'm more important. And you can see that.
{{< /lead >}}


### Usage

Simply place the following shortcode on the page

{{< code lang="html" >}}
{{</* lead */>}} [content] {{</* /lead */>}}
{{< /code >}}


## More information


### Theme directory tree

```
C:\
└───Hugo
    └───pCP-docs
        └───themes
            └───ace-documentation
                ├───archetypes
                ├───assets
                ├───layouts
                │   ├───downloads
                │   ├───partials
                │   ├───releases
                │   ├───shortcodes     <---- in here
                │   ├───taxonomy
                │   └───_default
                └───static
```

### Shortcodes available

{{< table style="table-striped" >}}
| Shortcode       | Description                             |
|----             |----                                     |
| alert.html      | Not used                                |
| button.html     | Not used                                |
| card.html       | Used for warning and danger message     |
| childpages.html | Used to generate links to child pages   |
| code.html       | Used to display code with a copy button |
| collapse.html   | Not used                                |
| columns.html    | Not used                                |
| details.html    | Not used                                |
| doublecode.html | Not used                                |
| lead.html       | Used for a lead in paragraph            |
| tab.html        | Not used                                |
| table.html      | Not used                                |
| tabs.html       | Not used                                |
{{< /table >}}

### Further reading

- [Hugo shortcodes](https://gohugo.io/content-management/shortcodes/)
