---
title: Raspberry Pi
description:
date: 2020-12-02
author: pCP Team
weight: 5
pcpver: "6.1.0"
toc: true
draft: false
tags:
- RPi
---

{{< lead >}}
The Raspberry Pi is a small and relatively cheap SBC (single board computer). First introduced in 2012, the range has regularly expanded each year, resulting in the many models and options now available. The Raspberry Pi has been very successful, selling millions every year.
{{< /lead >}}
{{< lead >}}
The Raspberry Pi makes an excellent hardware platform for piCorePlayer because of this success, product availability and abundant support.
{{< /lead >}}

![Raspberry Pi 4B](/components/raspberry_pi_4b.jpg)


## Models

{{< table style="table-striped" >}}
| Model      | Memory          | CPU                  |
|------------|-----------------|----------------------|
| Zero       | 512MB           | 1GHz - single core   |
| Zero W     | 512MB           | 1GHz - single core   |
|            |                 |                      |
| 1A         | 256MB           | 700MHz - single core |
| 1A+        | 256MB           | 700MHz - single core |
| 1B         | 256MB 512MB     | 700MHz - single core |
| 1B+        | 512MB           | 700MHz - single core |
|            |                 |                      |
| 2B         | 1GB             | 900MHz - quad core   |
|            |                 |                      |
| 3B         | 1GB             | 1.2GHz - quad core   |
| 3B+        | 1GB             | 1.4GHz - quad core   |
| 3A+        | 512MB           | 1.4GHz - quad core   |
|            |                 |                      |
| 4B         | 1GB 2GB 4GB 8GB | 1.5GHz - quad core   |
|            |                 |                      |
| 400        | 4GB             | 1.8GHz - quad core   |
|            |                 |                      |
| CM1        | 512MB           | 700MHz - single core |
| CM3        | 1GB             | 1.2GHz - quad core   |
| CM3+       | 1GB             | 1.2GHz - quad core   |
| CM4        | 1GB 2GB 4GB 8GB | 1.5GHz - quad core   |
{{< /table >}}

<br>

For currently available Raspberry Pi products---see [Raspberry Pi - Products](https://www.raspberrypi.org/products/)

## Recommendation

piCorePlayer will "normally" work on all Raspberry Pi's.

The Raspberry Pi Foundation has a policy of maintaining a US$35 price point, so the latest model with the minimum specs will be probably be the best value for money. At the time of writing that is a RPi4B 2GB.

Raspberry Pi Zeros are the cheapest by far, but they are usually in short supply and can be very difficult to purchase in some markets.

##### Considerations:

The faster the CPU:
- the quicker the pCP web GUI displays.
- the quicker LMS scans the media directories.
- the more power is consumed.
- the more heat is generated.

Other factors to consider:
- Local or LMS format encoding/decoding (ie. DSD256/DSD512)
- Local or LMS re-sampling
- LMS plugins used
- Number of players
- Number of players synched
- Size of music library
- File transfer speed
- USB devices
- 32bit vs 64bit


# More information

- [Raspberry Pi Foundation](https://www.raspberrypi.org/).
- [Raspberry Pi - wikipedia](https://en.wikipedia.org/wiki/Raspberry_Pi)
- [Raspberry Pi - Products](https://www.raspberrypi.org/products/)
