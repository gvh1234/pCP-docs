---
title: piCore
description:
date: 2020-11-21
author: pCP Team
weight: 6
pcpver: "6.1.0"
toc: true
draft: false
tags:
- piCore
---

{{< lead >}}
piCore is the Raspberry Pi port of Tiny Core Linux developed by Team Tiny Core. It is an independent system architected by Robert Shingledecker and now developed by a small team of developers with strong community support.
{{< /lead >}}
{{< lead >}}
Tiny Core Linux is not a traditional distribution but a toolkit to create your own customized system. It offers not only flexibility, small footprint but a very recent kernel and set of applications making it ideal for custom systems, appliances as well as to learn Linux, matching Raspberry Pi perfectly. It is running entirely in RAM. There is no installation in conventional terms; extensions mounted read only, after reboot the same clean system is available. Base raw SD card image with CLI version is only 21.5 Mbyte including RPi boot loader, firmware and support files.
{{< /lead >}}

Main features:

- Kernel with sound DAC support
- BusyBox
- eglibc
- Xorg
- zswap (compressed swap in RAM), zram (compressed RAM)
- Linaro (GCC) toolchain
- Common TC base


## More information

- [Tiny Core](http://www.tinycorelinux.net/intro.html)
- [Towards Core (microcore) on Raspberry Pi](http://forum.tinycorelinux.net/index.php/topic,13982.0.html)
- [piCore (Tiny Core) Linux on Raspberry Pi](https://iotbyhvm.ooo/picore-tiny-core-linux-on-raspberry-pi/)
