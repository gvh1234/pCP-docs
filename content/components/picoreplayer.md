---
title: piCorePlayer
description:
date: 2020-07-26T17:50:45+10:00
author: pCP Team
weight: 1
pcpver: "5.0.0"
toc: true
draft: false
tags:
- pCP
---

piCorePlayer is a software emulation of a Squeezebox network music player that runs exclusively on the Raspberry Pi.

piCorePlayer is designed as a headless music player, much like the Squeezebox Duet. Optionally, a keyboard and monitor or touch screen can be added if desired.

piCorePlayer uses piCore Linux, a very small embedded Linux distribution that runs in RAM. After the initial setup, it only reads the SD card. This gives you a very robust system that, like most embedded systems, is very unlikely to corrupt your SD card. This enables you can simply cut the power without the need of a complex shutdown procedure. For exception---see [Warning](#warning) below.

piCorePlayer is very small distribution, being only about 75 MB in total, including the operating system (piCore Linux). It boots very fast, ready for use in about 30 seconds after you power on. Your piCorePlayer will be automatically found by the Logitech Media Server (LMS). It is possible to synchronize piCorePlayer to your other Squeezebox devices.


## piCorePlayer can be used in 4 ways

1. As a Squeezebox network music player that connects to your existing Logitech Media Server (LMS).
2. As a Logitech Media Server (LMS).
3. As a combined Squeezebox music player and Logitech Media Server (LMS).
4. As a controller.

{{< card style="warning" >}}
Logitech Media Server (LMS) is a server database application and needs to be shutdown properly:
- Do **not** just pull the power plug.
- Use [Main Page] > [Shutdown].{{< /card>}}


## Features of piCorePlayer

- Uses embedded piCore Linux.
- Uses Squeezelite for Squeezebox emulation.
- Web based management interface for user configuration.
- Supports the Official 7" Raspberry Pi Touch screen through jivelite.
- Command line setup script for situations where the network is not available.
- SSH access using OpenSSH for secure connection via LAN from another computer (ie. via PuTTY).
- DHCP for wired ethernet (default setting).
- Supports most USB Wifi adapters.
- Supports Analog out via the 3.5mm audio jack (default setting).
- Supports HDMI audio up to 24 bit/192 khz.
- Supports I2S to Analog out.
- Supports I2S to S/PDIF out.
- Supports USB DACs up to 24 bit/192 khz.
- Supports various Infrared Remotes via LIRC.
- Supports additional extensions, such as LMS, Shairport-sync, ALSAequal and nano.
- Compatible with your existing Logitech Squeezebox devices.
- piCorePlayer CLI for enhanced command line and scripting.
