---
title: SD card
description:
date: 2021-01-24
author: pCP Team
weight: 7
pcpver: "7.0.0"
toc: true
draft: false
tags:
- SD card
---


{{< lead >}}
What SD card do you need for piCorePlayer?
{{< /lead >}}

![SD card](/components/sd-card.png)


## Recommendation

- Get the smallest SD card available.
- Invest in a quality brands SD card from a reliable supplier.
- Standard speed SD cards are all that is really required.
- No need for industrial quality SD cards.


## piCorePlayer is very small

At the time of writing, piCorePlayer's initial image contains 2 x 64MB partitions, so the  total size is 128MB. That's very small for Linux distribution and audio player software.

- If you want to load a couple of extra features, you may need to increase the second partition (root partition) a hundred megabytes or so.
- If you want to use piCorePlayer for LMS, you will need to increase the second partition a gigabyte or two.


## SD cards are big

At the time of writing, the smallest SD card commonly available is 16GB. This is vastly more disk space than required.

### Standard piCorePlayer installation

You can see from the following pie chart, piCorePlayer takes up less than 1% of the SD card.

<div class="mermaid">
	pie
	"Free - ~16GB" : 16000000000
	"pCP - 128MB" : 128000000
</div>

### LMS piCorePlayer installation with 1GB second partition

Even with 1GB for LMS you can see that only 7% of the SD card is used.

<div class="mermaid">
	pie
	"Free - ~15GB" : 16000000000
	"pCP - 128MB" : 128000000
	"LMS - 1GB" : 1000000000
</div>


## SD card speed

Once setup, piCorePlayer does very little, if any, writing to the SD card. So a normal speed SD card is all that is really required.

If you have LMS installed (LMS writes cache to the SD card), a faster write SD card will speed up some functions but I don't think you will notice a huge difference in day to day operation.


## Fake SD cards

I always bought SanDisk SD cards and have never had a failure. I suspect a lot of the reported SD card problems on the Raspberry Pi may be a result of people buying fake SD cards from unknown sellers.

SD cards do eventually wear out, so be cautious if you repurposing an old SD card.

Avoid the pain, only buy quality SD cards from a well known, reputable supplier.


## SD card reliability

Genuine, quality, branded SD cards have proven very reliable. I don't think there is a valid reason to buy an expensive industrial SD cards.

## Adding a third partition

- [Add a third partition on a SD card](/how-to/add_third_partition/)

## More information

- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
