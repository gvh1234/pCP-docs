---
title: Logitech Media Server
description: Logitech Media Server (LMS)
date: 2020-11-21
author: pCP Team
weight: 3
pcpver: "6.1.0"
toc: true
draft: false
tags:
- LMS
---

Logitech Media Server (LMS) is the server software that powers networked audio players from Logitech including Squeezebox, Radio, Boom, Receiver, Transporter and various third party hardware devices that use Squeezelite or Squeezeplay.

Logitech Media Server (LMS) is Open Source Software written in Perl and it runs on pretty much any platform that Perl runs on, including Linux, Mac OSX, Solaris and Windows.

Logitech Media Server (LMS) was previously known as Squeezebox Server, SqueezeCentre and SlimServer.


## Installation
Logitech Media Server (LMS) can be installed on piCorePlayer via the [LMS] page on the piCorePlayer web interface.

{{< card style="info" >}}The default version of LMS will be 7.9.3. If you would like to move to 8.0.0. for example---see [Upgrade Logitech Media Server](/how-to/upgrade_lms/){{< /card >}}

{{< card style="warning" >}}
Logitech Media Server (LMS) is a server database application and needs to be shutdown properly:
- Do **not** just pull the power plug.
- Use [Main Page] > [Shutdown].{{< /card>}}


## More information

- [Logitech Media Server (LMS)](http://wiki.slimdevices.com/index.php/Logitech_Media_Server)
- [Logitech Media Server (LMS) v7.9.3 source](http://downloads.slimdevices.com/LogitechMediaServer_v7.9.3/)
- [Manually upgrade Logitech Media Server](/how-to/upgrade_lms/)
