---
title: Jivelite
description:
date: 2021-01-24
author: pCP Team
weight: 4
pcpver: "5.0.0"
toc: true
draft: false
tags:
- Jivelite
---

Jivelite is a cut down Squeezebox control application derived from Logitech's SB Jive.

- Originally developed and released by Adrian Smith.
- Currently maintained by Ralph Irving.

piCorePlayer fully supports Jivelite on the Raspberry Pi Foundation's Official 7" Touch Screen.



## Installation

Jivelite can be installed on piCorePlayer via the [Tweaks] page on the piCorePlayer web interface.


## Jivelite [Settings] > [piCorePlayer] screen

- Reboot piCorePlayer
- Shutdown piCorePlayer
- Rescan LMS Media Library
- Adjust Display Backlight Brightness
- Adjust Display Backlight Brightness when Powered Off
- Enable Power On Button when Powered Off
- Wake-on-LAN
- Save Settings to SD Card


## More information

- [Announce: JiveLite - cut down squeezebox control application - Triode](http://forums.slimdevices.com/showthread.php?98156)
- [Jivelite for piCorePlayer - Ralph Irving](http://forums.slimdevices.com/showthread.php?103330-Jivelite-for-piCorePlayer)
- [Original Jivelite - Adrian Smith - Google Code Archive](https://code.google.com/archive/p/jivelite/)
- [Latest Jivelite - Ralph Irving - github](https://github.com/ralph-irving/jivelite)
- [Jivelite for piCorePlayer - Ralph Irving - github](https://github.com/ralph-irving/tcz-jivelite)
