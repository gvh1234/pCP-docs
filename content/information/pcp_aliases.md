---
title: piCorePlayer aliases
description:
date: 2020-07-24T07:17:32+10:00
author: pCP Team
weight: 20
pcpver: "5.0.0"
toc: true
draft: false
tags:
- CLI
---

To speed up some common Linux command line tasks we have defined a few aliases.

| **Alias** | **Description**                                  |
|-----------|--------------------------------------------------|
| ce        | Change directory to /mnt/mmcblk0p2/tce
| ceo       | Change directory to /mnt/mmcblk0p2/tce/optional
| m1        | Mount the boot partition /mnt/mmcblk0p1
| m2        | Mount the second partition /mnt/mmcblk0p2
| c1        | Change directory to /mnt/mmcblk0p1
| c2        | Change directory to /mnt/mmcblk0p2
| vicfg     | Edit configuration file config.txt using vi
| vicmd     | Edit boot file cmdline.txt using vi
| u1        | Unmount the boot partition /mnt/mmcblk0p1
| u2        | Unmount the second partition /mnt/mmcblk0p2

<br>
{{< card title="Note:" style="danger" >}}A complete list of aliases is shown below—accurate at the time of writing.{{< /card >}}

Use the Linux "alias" command to show what is available on your piCorePlayer---see [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh)

```
ls='ls -p'
cp='cp -i'
vicmd='vi /mnt/mmcblk0p1/cmdline.txt'
df='df -h'
u1='umount /mnt/mmcblk0p1'
u2='umount /mnt/mmcblk0p2'
c1='cd /mnt/mmcblk0p1'
c2='cd /mnt/mmcblk0p2'
rm='rm -i'
mv='mv -i'
la='ls -la'
du='du -h'
ceo='cd /mnt/mmcblk0p2/tce/optional'
ce='cd /mnt/mmcblk0p2/tce'
m1='mount /mnt/mmcblk0p1'
m2='mount /mnt/mmcblk0p2'
ll='ls -l'
vicfg='vi /mnt/mmcblk0p1/config.txt'
```
