---
title: Glossary
description:
date: 2020-11-21
author: pCP Team
weight: 10
pcpver: "6.1.0"
toc: true
draft: false
---

### ALSAequal

An Equalisier that works with ALSA---see [the C\* Audio Plugin Suite](http://www.quitte.de/dsp/caps.html).

### jivelite

A touch screen graphical user interface that works like the Logitech Squeezebox Touch.

A community Logitech Media Server (LMS) control application---see [Jivelite](/components/jivelite/).

### LMS

Logitech Media Server (formally Slimserver, SqueezeCentre and Squeezebox Server)---see [LMS](/components/lms/).

### pCP

piCorePlayer---see [piCorePlayer](/components/picoreplayer/).

### piCore

A version of Tiny Core Linux for the Raspberry Pi---see [piCore](/components/picore/)

### piCorePlayer

Squeezebox replacement using Raspberry Pi + piCore + squeezelite + web interface---see [piCorePlayer](/components/picoreplayer/).

### putty

Software to connect to your piCorePlayer in terminal mode.

### Raspberry Pi

Cheap credit card sized computer---see [Raspberry Pi Foundation](https://www.raspberrypi.org/).

The Raspberry Pi makes an excellent hardware platform for piCorePlayer.

### SD card

Secure Digital (SD) card is a non-volatile memory card format---see [Wikipedia - Secure Digital](https://en.wikipedia.org/wiki/Secure_Digital).

### Squeezelite

Squeezelite is a headless Squeezebox emulator for Linux/OSX/Windows---see [Squeezelite](/components/squeezelite/).

### ssh

Software to connect to your piCorePlayer in terminal mode.
