---
title: piCorePlayer shutdown
description:
date: 2021-01-30
author: pCP Team
weight: 50
pcpver: "7.0.0"
toc: true
draft: true
tags:
- Shutdown
---

{{< lead >}}
How to do a **shutdown** on piCorePlayer. piCorePlayer "shutdowns" usually activate the following piCore command `exitcheck.sh shutdown`.
{{< /lead >}}


{{< card style="warning" >}}
Logitech Media Server (LMS) is a server database application and needs to be shutdown properly:
- Do **not** just pull the power plug.
- Do a proper shutdown.
{{< /card>}}

{{< card style="info" >}}
The following shutdown methods do the same thing.
{{< /card >}}

## Web interface

The `shutdown` command can be found on the [Main Page] of the piCorePlayer web interface.

- Click [Main Page] > [Shutdown] button on web interface.


## Jivelite

To do a shutdown from Jivelite, use the `piCorePlayer` settings screen.

- Select [Settings] > [piCorePlayer] > "Shutdown piCorePlayer" on the Jivelite screen.

![Jivelite Settings Screen](/information/jivelite_settings.png)

{{< card style="info" >}}
Jivelite's power button does not do a piCorePlayer shutdown.
{{< /card >}}


## Command line interface (CLI)

- Type `pcp sd` on the command line for shutdown, or
- Type `pcp bs` on the command line for a backup followed by a shutdown, or
- Type `exitcheck.sh shutdown`.


## Setup CLI interface

- Type `setup`
- Select [Main] > [Shutdown]


## Hardware shutdown via GPIO

CHECK: They will probably do a "Raspberry Pi OS" type shutdown rather than "piCore" type shutdown.

- gpio-shutdown
- gpio-poweroff


### gpio-shutdown

```text
Name:   gpio-shutdown
Info:   Initiates a shutdown when GPIO pin changes. The given GPIO pin
        is configured as an input key that generates KEY_POWER events.

        This event is handled by systemd-logind by initiating a
        shutdown. Systemd versions older than 225 need an udev rule
        enable listening to the input device:

                ACTION!="REMOVE", SUBSYSTEM=="input", KERNEL=="event*", \
                        SUBSYSTEMS=="platform", DRIVERS=="gpio-keys", \
                        ATTRS{keys}=="116", TAG+="power-switch"

        Alternatively this event can be handled also on systems without
        systemd, just by traditional SysV init daemon. KEY_POWER event
        (keycode 116) needs to be mapped to KeyboardSignal on console
        and then kb::kbrequest inittab action which is triggered by
        KeyboardSignal from console can be configured to issue system
        shutdown. Steps for this configuration are:

            Add following lines to the /etc/console-setup/remap.inc file:

                # Key Power as special keypress
                keycode 116 = KeyboardSignal

            Then add following lines to /etc/inittab file:

                # Action on special keypress (Key Power)
                kb::kbrequest:/sbin/shutdown -t1 -a -h -P now

            And finally reload configuration by calling following commands:

                # dpkg-reconfigure console-setup
                # service console-setup reload
                # init q

        This overlay only handles shutdown. After shutdown, the system
        can be powered up again by driving GPIO3 low. The default
        configuration uses GPIO3 with a pullup, so if you connect a
        button between GPIO3 and GND (pin 5 and 6 on the 40-pin header),
        you get a shutdown and power-up button. Please note that
        Raspberry Pi 1 Model B rev 1 uses GPIO1 instead of GPIO3.
Load:   dtoverlay=gpio-shutdown,<param>=<val>
Params: gpio_pin                GPIO pin to trigger on (default 3)
                                For Raspberry Pi 1 Model B rev 1 set this
                                explicitly to value 1, e.g.:

                                    dtoverlay=gpio-shutdown,gpio_pin=1

        active_low              When this is 1 (active low), a falling
                                edge generates a key down event and a
                                rising edge generates a key up event.
                                When this is 0 (active high), this is
                                reversed. The default is 1 (active low).

        gpio_pull               Desired pull-up/down state (off, down, up)
                                Default is "up".

                                Note that the default pin (GPIO3) has an
                                external pullup. Same applies for GPIO1
                                on Raspberry Pi 1 Model B rev 1.

        debounce                Specify the debounce interval in milliseconds
                                (default 100)
```


### gpio-poweroff

```text
Name:   gpio-poweroff
Info:   Drives a GPIO high or low on poweroff (including halt). Enabling this
        overlay will prevent the ability to boot by driving GPIO3 low.
Load:   dtoverlay=gpio-poweroff,<param>=<val>
Params: gpiopin                 GPIO for signalling (default 26)

        active_low              Set if the power control device requires a
                                high->low transition to trigger a power-down.
                                Note that this will require the support of a
                                custom dt-blob.bin to prevent a power-down
                                during the boot process, and that a reboot
                                will also cause the pin to go low.
        input                   Set if the gpio pin should be configured as
                                an input.
        export                  Set to export the configured pin to sysfs
        timeout_ms              Specify (in ms) how long the kernel waits for
                                power-down before issuing a WARN (default 3000).
```


## More information

- 
