---
title: piCorePlayer backup
description:
date: 2020-10-29
author: pCP Team
weight: 40
pcpver: "6.1.0"
toc: true
draft: false
tags:
- Backup
---

{{< lead >}}
The "backup" on piCorePlayer is not a backup in the normal sense. As piCorePlayer is essentially a readonly RAM based system, it needs a mechanism to save the configuration files so the changes can be restored after a reboot. To achieve this, the piCore includes a program called  `filetool.sh` that allows for a selective backup of files and directories that are listed in the file `/opt/.filetool.lst`---see [Typical .filetool.lst](#typical-filetoollst)
{{< /lead >}}
{{< lead >}}
The default backup file is `/mnt/mmcblk0p2/tce/mydata.tgz` and is stored directly on the SD card in a persistent location. The files stored in the backup are automatically restored every time piCorePlayer reboots. This backup is not done for safety reasons, it is an essential part of the boot process.
{{< /lead >}}

The backup command will backup the following:

- configuration files.
- home directory.
- as well as other files and directories listed in `/opt/.filetool.lst`---see [Typical .filetool.lst](#typical-filetoollst)

A backup can be done through the piCorePlayer web interface, Jivelite or the command line interface.

{{< card style="info" >}}
All the following backup methods do the same thing.
{{< /card >}}

## Web interface

piCorePlayer, through its web interface, automatically does a backup after it changes any of the settings, so an additional backup is not usually required.

The backup command can be found on the [Main page] of the piCorePlayer web interface.

- Click [Main page] > [Backup] button on web interface.


## Jivelite

Jivelite does not automatically save Jivelite configuration changes, so you must do a backup.

- Select [Settings] > [piCorePlayer] > "Save Settings to SD Card" on the Jivelite screen.


## Command line interface (CLI)

If there has been changes done to configuration files or other changes you must do a backup to make the changes persistent. If the changes lie outside the directories or files defined in `/opt/.filetool.lst` then `/opt/.filetool.lst` must be updated before a backup is done.

- Type `pcp bu` on the command line.


## Typical .filetool.lst

$ `cat /opt/.filetool.lst`
```
opt
home
etc/asound.conf
etc/group
etc/gshadow
etc/httpd.conf
etc/passwd
etc/shadow
usr/local/etc/pointercal
usr/local/etc/ssh/ssh_host_dsa_key
usr/local/etc/ssh/ssh_host_dsa_key.pub
usr/local/etc/ssh/ssh_host_ecdsa_key
usr/local/etc/ssh/ssh_host_ecdsa_key.pub
usr/local/etc/ssh/ssh_host_ed25519_key
usr/local/etc/ssh/ssh_host_ed25519_key.pub
usr/local/etc/ssh/ssh_host_rsa_key
usr/local/etc/ssh/ssh_host_rsa_key.pub
usr/local/etc/init.d/pcp_startup.sh
usr/local/etc/pcp
var/lib/alsa/asound.state
var/spool/cron/crontabs
```

{{< card style="warning" >}}There is no leading "/" as filetool.sh uses the tar command to create the backup.{{< /card >}}


## More information

- Default backup file /mnt/mmcblk0p2/tce/mydata.tgz
- All the backup methods do the same thing.
