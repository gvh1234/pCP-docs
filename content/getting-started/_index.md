---
title: Getting started
description:
date: 2020-07-26T09:08:03+10:00
author: pCP Team
weight: 10
pcpver: "5.0.0"
toc: true
draft: false
categories:
- Setup
- How to
---

## Get piCorePlayer up and running on your Raspberry Pi

By default, if you are using wired ethernet and DHCP, piCorePlayer will just work through the 3.5mm audio jack on the Raspberry Pi without any user setup. It will appear on Logitech Media Server (LMS) as player called piCorePlayer. So, plug in your headphones, select a track and press play.

{{< card style="info" >}}Some of the Raspberry Pi's do not have an 3.5mm audio jack.{{< /card >}}

##### Step 1

Download piCorePlayer---see [Download piCorePlayer](/how-to/download_picoreplayer/).

##### Step 2

Burn image to a SD card---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).

##### Step 3

Insert the SD card and LAN cable into Raspberry Pi, then apply power.

##### Step 4

A piCorePlayer should appear in LMS named "piCorePlayer". It can be controlled just like any other Squeezebox player, via:

- Logitech Media Server (LMS) web interface via any browser (ie. Material skin)
- Software on your smartphone (ie. iPeng)
- Duet Controller
- Squeezebox Radio
- Squeezebox Touch
- piCorePlayer Simple Controls
- piCorePlayer with jivelite
- Squeezeslave on PC

##### Step 5

{{< card style="info" >}}Some of the smaller Raspberry Pi's do not have an 3.5mm audio jack. In that case you will have to jump this step.{{< /card >}}

Test "Analog audio" works.

By default, analog audio via the 3.5mm audio jack will be working. So, plug in your headphones, select a track and press play.

##### Step 6

Access the piCorePlayer web interface via the IP address shown on the boot screen---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).

Type `http://your_IP_address` into your browser.

##### Step 7

Select [Squeezelite Settings] > Choose audio output > Audio output.

Click [Save].

Click [Yes] when requested to "Reboot piCorePlayer".

##### Step 8

Select [Squeezelite Settings] > Change Squeezelite settings > Name of your player.

Type player name.

Click [Save].

Click [Yes] when requested to "Restart Squeezelite".

##### Step 9

Select [Tweaks] > pCP System Tweaks > Host name.

Type host name.

Click [Save].

Click [Yes] when requested to "Reboot piCorePlayer".


## More information

- [Raspberry Squeezie](/projects/raspberry-squeezie/)
