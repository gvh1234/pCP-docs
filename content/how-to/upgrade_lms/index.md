---
title: Upgrade LMS
description: Upgrade Logitech Media Server (LMS)
date: 2020-07-24T08:48:27+10:00
author: pCP Team
weight: 90
pcpver: "5.0.0"
toc: true
draft: false
categories:
- Setup
- How to
tags:
- LMS
---

It's easy to install Logitech Media Server on your piCorePlayer using its settings menus. As of this writing piCorePlayer is installing Logitech Media Server 8.0.0. This is a release branch that does not get nightly updates.  If you want to select stable bugfix or development branches, then you can follow these instructions.

## Step-by-step instructions

##### Step 1

Access piCorePlayer via ssh---see [Access piCoreplayer via ssh](/how-to/access_pcp_via_ssh/).

##### Step 2

$ `cd /tmp`

##### Step 3

$ `wget https://raw.githubusercontent.com/piCorePlayer/lms-update-script/Master/lms-update.sh`

##### Step 4

$ `chmod 755 lms-update.sh`

##### Step 5

To select and install the current release branch of LMS.\
$ `sudo ./lms-update.sh --release release -s -r -u` 

-or-

To select the stable branch with nightly bugfix updates of LMS.\
$ `sudo ./lms-update.sh --release stable -s -r -u` 

-or-

To select the current unstable development branch with nightly updates.\
$ `sudo ./lms-update.sh --release devel -s -r -u` 

## More information

- [Install LMS](/how-to/install_lms/)
