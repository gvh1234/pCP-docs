---
title: Add a USB hard drive
description: Add a USB hard drive and setup SAMBA
date: 2020-12-22
author: nowhinjing
weight: 100
pcpver: "5.0.0"
toc: true
draft: false
categories:
- Setup
- How to
tags:
- LMS
- USB HDD
- SAMBA
---

## Step 1 - Adding an USB Hard Disk - Preparation

If the USB hard disk you are adding is formatted as FAT32 or NTFS you will need to install the "additional Filesystems pack" before you can load and configure the disk.

![install additional filesystems](USB_10.png)

Note that this step is not required if your disk is formatted as EXT4. Windows users can pre-format such a disk using the free utility **MiniTool Partition Manager**, and this is in fact what I have done.

`https://www.partitionwizard.com/free-partition-manager.html`

![minitool partion wizard free](USB_11.png)

Appropriate directories can then be set up on the EXT4 disk from Windows using the free version of **Extfs for Windows** from Paragon software.

`http://www.paragon-drivers.com/extfs-windows/galleryImage.html`

In any case you will need to set up two directories on your USB hard disk for later use by LMS, these directories that will contain your music and playlists; I have imaginatively set them up as "music" and "playlists" (ahem!).

Now is a good time to backup and reboot your piCorePlayer, so return to the "Main Page" and then "Backup" and "Shutdown"

![backup and shutdown pcp](USB_12.png)


## Step 2 - Adding an USB Hard Disk - Execution

Remove the power from your Raspberry Pi, Plug-in the USB disk, re-apply the power and wait for your Pi to boot up and connect to the network.

Start the browser, and enter the IP address assigned to the Pi, this will usually be the same as the address you used previously, but if not use **Advanced IP Scanner** to obtain the new address as previously described. You should now be back at the "Welcome" or "Main Page" screen.

In either case, select "LMS" from the menu bar, and scroll down to the "Pick from the following detected USB disks to mount" section. You should note that your USB disk has been automatically detected. All you have to do now is to name the mount point [in my case /mnt/LMSfiles], click on the "Enabled" radio button and then click on "Set USB Mount".

![mount usb disk](USB_13.png)

When this has completed, return to the previous [LMS] page. Scroll down to "Save LMS Server Cache and Preferences to Mounted Drive" click on the "Enabled" radio button next to USB Disk and click on "Move LMS Data"

![move lms cache](USB_14.png)

When this has completed, return to the previous [LMS] page. In "Logitech Media Server (LMS) operations" select "Configure LMS".

![configure lms](USB_15.png)

Unless you wish to set up a mysqueezebox account, skip the first LMS set up screen, then select your music and playlist locations on your USB disk.

![setup lms folders](USB_16.png)

Having completed the setup, you should now be able to play audio from the piCoreLMS to the internal piCorePlayer.

Now is a good time to backup and reboot your piCorePlayer, so return to the "Main Page" and then "Backup" and "Shutdown"

![backup and shutdown pcp](USB_12.png)


## Step 3 - Install and Configure Samba

Apply power to your Raspberry Pi and wait for it to boot up and connect to the network.

Start the browser, and enter the IP address assigned to the Pi, this will usually be the same as the address you used previously, but if not use **Advanced IP Scanner** to obtain the new address as previously described. You should now be back at the "Welcome" or "Main Page" screen.

In either case, select "LMS" from the menu bar, and scroll down to the "Setup Samba Share" section, and click on "Install".

![install samba](USB_17.png)

When this has completed, scroll down to the "Setup Samba Share" section and enter values for "Server Name", piCoreLMS; "Share Name", LMSfiles; "Share Path", /mnt/LMSfiles; and "Create File Mode", _0775_; then click on "Set Samba"

![set samba parameters](USB_18.png)

When this has completed, return to the previous [LMS] page. Scroll down to the "Setup Samba Share" section and enter a value for "Password", then click on "Set Password".

![set samba password](USB_19.png)

When this has completed, return to the previous [LMS] page.

Now is a good time to backup and reboot your piCorePlayer, so return to the "Main Page" and then "Backup" and "Shutdown"

![backup and shutdown pcp](USB_12.png)


## Step 4 - Hooray ! It all works ...

Apply power to your Raspberry Pi and wait for it to boot up and connect to the network.

Start the browser, and enter the IP address assigned to the Pi, this will usually be the same as the address you used previously, but if not use **Advanced IP Scanner** to obtain the new address as previously described. You should now be back at the "Welcome" or "Main Page" screen.

On your Windows PC, launch Windows Explorer. Right-click on ThisPC and select "Add a network location". Enter the address "\\\\piCoreLMS\\LMSfiles" and then username "tc" and the password that you assigned to Samba at the end of step 6. You should now be able to access the USB disk from the Windows PC.

![files available over samba](USB_20.png)


## More information

- [Advanced IP Scanner - Windows](http://www.advanced-ip-scanner.com/)
- [nmap - Linux, Mac OS X and Windows](https://nmap.org/)
