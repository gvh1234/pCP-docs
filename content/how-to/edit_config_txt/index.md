---
title: Edit config.txt
description:
date: 2020-07-24T08:46:16+10:00
author: pCP Team
weight: 40
pcpver: "5.0.0"
toc: true
draft: false
categories:
- Setup
- How to
- Configure
---

config.txt is found on piCorePlayer's boot partition /mnt/mmcblk0p1

By default, after booting, the boot partition is unmounted. So to edit config.txt, you must first mount the boot partition, then edit using vi.

{{< card style="warning" >}}You must reboot piCorePlayer after changing config.txt to activate changes.{{< /card >}}


## Longhand method—using standard Linux commands

##### Step 1

Access piCorePlayer via ssh---see [Access piCoreplayer via ssh](/how-to/access_pcp_via_ssh/).

##### Step 2

$ `mount /mnt/mmcblk0p1`

##### Step 3

$ `cd /mnt/mmcblk0p1`

##### Step 4

$ `vi config.txt`

##### Step 5

$ `reboot`


## Shorthand method—using pCP aliases

##### Step 1

Access piCorePlayer via ssh---see [Access piCoreplayer via ssh](/how-to/access_pcp_via_ssh/).

##### Step 2

$ `m1`

##### Step 3

$ `c1`

##### Step 4

$ `vicfg`

##### Step 5

$ `pcp rb`


## More information

- [piCorePlayer aliases](/information/pcp_aliases/)
- [piCorePlayer CLI](/information/pcp_cli/)
- [Basic vi commands](https://www.cs.colostate.edu/helpdocs/vi.html)
- [Raspberry Pi config.cfg](http://elinux.org/index.php?title=RPiconfig)
