---
title: Setup passwordless ssh
description: Setup passwordless ssh access
date: 2020-12-11
author: pCP Team
weight: 120
pcpver: "6.1.0"
toc: true
draft: false
categories:
- Setup
tags:
- ssh
---

{{< lead >}}
Setup ssh access to piCorePlayers without prompting for passwords. This is useful if you need to use scripts that interact with remote piCorePlayers.
{{< /lead >}}

##### Step 1
Determine the \<IP address\> of the remote computer---see [Determine your pCP IP address](/how-to/determine_your_pcp_ip_address/).

##### Step 2a
Create the public/private authentication keys using \<Enter\> to accept the default values.

$ `ssh-keygen -t ed25519`

Valid encryption types:
- dsa
- rsa
- ed25519

##### Step 2b
Or, create the public/private authentication keys non-interactively.

$ `ssh-keygen -t ed25519 -N "" -f "/home/tc/.ssh/id_ed25519"`

Valid encryption types:
- dsa
- rsa
- ed25519

##### Step 3
Transfer the public authentication key to the remote computer using the following command. The ssh-copy-id command is not available on piCore. You will need to type the remote computer's tc password when prompted.

$ `cat ~/.ssh/id_ed25519.pub | ssh tc@<IP address> 'mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys'`

If prompted:
```text
The authenticity of host '<IP address> (<IP address>)' can't be established.
ECDSA key fingerprint is SHA256:tnQbsvd9F3mTRxdfggQ2utEUoaEpy2hvMHrd5FU9D/U.
Are you sure you want to continue connecting (yes/no)?
```
Type `yes`.
```text
Warning: Permanently added '<IP address>' (ECDSA) to the list of known hosts.
```

##### Step 4
Connect to the remote computer.

$ `ssh tc@<IP address>`

##### Step 5a
If prompted:
```text
The authenticity of host '<IP address> (<IP address>)' can't be established.
ECDSA key fingerprint is SHA256:tnQbsvd9F3mTRxdfggQ2utEUoaEpy2hvMHrd5FU9D/U.
Are you sure you want to continue connecting (yes/no)?
```
Type `yes`.
```text
Warning: Permanently added '<IP address>' (ECDSA) to the list of known hosts.
```

##### Step 5b
If prompted:

```text
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ECDSA key sent by the remote host is
SHA256:t7jT8POQnW5XPabyucIE1n5HjSz6CwYjeCA+CiBg.
Please contact your system administrator.
Add correct host key in /home/tc/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in /home/tc/.ssh/known_hosts:8
ECDSA host key for <IP address> has changed and you have requested strict checking.
Host key verification failed.
```

$ `ssh-keygen -R <IP address>`
```text
# Host <IP address> found: line 8
/home/tc/.ssh/known_hosts updated.
Original contents retained as /home/tc/.ssh/known_hosts.old
```

Go back to **Step 4**.

##### Step 6
Do a backup on the remote computer.

$ `pcp bu`

##### Step 7
Close connection to the remote computer and return to the local computer.

$ `exit`

##### Step 8
Do a backup on the local computer.

$ `pcp bu`


## More information

- [Determine your pCP IP address](/how-to/determine_your_pcp_ip_address/)
- [Access pCP via ssh](/how-to/access_pcp_via_ssh/)
- [piCorePlayer aliases](/information/pcp_aliases/)
- [piCorePlayer CLI](/information/pcp_cli/)
