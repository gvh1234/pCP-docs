---
title: Download pCP
description: Download piCorePlayer
date: 2020-08-08
author: pCP Team
weight: 10
pcpver: "6.1.0"
draft: false
categories:
- Setup
- How to
tags:
- Download
---

{{< card style="info" >}}The download process can vary depending on operating system and web browser used.{{< /card >}}

##### Step 1

Go to the piCorePlayer Downloads web page---go to [Downloads](/downloads/).

##### Step 2

Click on the "piCorePlayer X.X.X - Standard Version" link to begin download.
{{< card style="warning" >}}The Experimental RealTime Kernel version is not recommended for general use.{{< /card >}}

##### Step 3

Wait for the download to begin---depends on browser.

##### Step 4

Click on the [Save] or [Save as] button if prompted---depends on browser.

##### Step 5

Wait for the download to complete---about 20 or 30 seconds depending on your internet speed.

##### Step 6

Go to your local download directory.

##### Step 7

Unzip the piCorePlayer image.

{{< card style="warning" >}} Some SD card creators work with compressed files, but the additional md5 checksum file may cause issues.{{< /card >}}


## More information

- The latest version of piCorePlayer is at the top of the [Downloads](/downloads/) page.
- The Experimental RealTime Kernel version is not recommended for general use.
- The download process can vary depending on operating system and web browser used.
- The downloaded zip file contains 2 files:
    - piCorePlayerx.x.x.img---this is the piCoreplayer image.
    - piCorePlayerx.x.x.img.md5.txt---this is the image's md5 checksum file.
    - Some OS hide the .img and .txt file extensions.
- Some SD card creators can work directly with compressed files but the additional md5 checksum file may cause issues.
