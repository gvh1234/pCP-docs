---
title: Determine your pCP IP address
description: Determine your piCorePlayer IP address
date: 2020-07-24T08:47:58+10:00
author: pCP Team
weight: 30
pcpver: "5.0.0"
toc: true
draft: false
categories:
- Setup
- How to
---

## Boot console

##### Step 1

Look at the boot console.

The IP address will be displayed at the end of the boot process.

![boot screen](pcp_ip_address_bootlog.png)

## IP scanner

##### Step 1

Use an IP scanner on your computer.

![IP scanner](pcp_ip_address_scanner.png)

## Router

##### Step 1

Check your router to see what IP address has been assigned.

DHCP on your router will have assigned an IP address from the defined DHCP range.

Check your router manual for instructions.

## More information

- [Advanced IP Scanner - Windows](http://www.advanced-ip-scanner.com/)
- [nmap - Linux, Mac OS X and Windows](https://nmap.org/)
