---
title: Add USB Ethernet adapter
description:
date: 2021-01-02
author: pCP Team
weight: 110
pcpver: "6.1.0"
toc: true
draft: false
categories:
- Setup
tags:
- USB
- Ethernet
---

{{< lead >}}
piCorePlayer includes the extension `net-usb-KERNEL.tcz` on the pCP image for those occasions when you have a RPi with a spare USB port but no Ethernet port and want to connect to a wired network. By default this extension is not loaded.
{{< /lead >}}
{{< lead >}}
To facilitate a headless install of `net-usb-KERNEL.tcz`, piCorePlayer looks for a file named `netusb` on the boot partition.
{{< /lead >}}

![USB Ethernet adapter](usb_ethernet_adatper.png)

##### Step 1

- Add an empty file "netusb" to boot partition.
- This will instruct piCorePlayer to load the appropriate firmware during the boot process.

##### Step 2

- Insert the USB Ethernet Adapter into the RPi's USB port.

##### Step 3

- Plug in the ethernet cable into the USB Ethernet Adapter.

##### Step 4

- Turn on the power to boot the RPi.


## Firmware

Firmware available in the extension net-usb-5.4.51-pcpCore-v8.tcz

```text
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/int51x1.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/sr9700.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/cdc_eem.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/kaweth.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/rtl8150.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/rndis_host.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/mcs7830.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/cdc_mbim.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/hso.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/sr9800.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/asix.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/lg-vl600.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/catc.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/ax88179_178a.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/ipheth.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/cx82310_eth.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/smsc75xx.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/net1080.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/huawei_cdc_ncm.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/cdc_subset.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/zaurus.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/pegasus.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/dm9601.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/sierra_net.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/cdc_ether.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/cdc_ncm.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/gl620a.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/plusb.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/kalmia.ko
usr/local/lib/modules/5.4.51-pcpCore-v8/kernel/drivers/net/usb/qmi_wwan.ko
```


## Diagnostics

Plug in the USB ethernet adapter and it will probably be automatically detected. View dmesg to determine if it was recognised---see [Main page] > [Diagnostics] > [Logs] > "dmesg" > [Show].

For "1 Port USB Network with 3 USB Ports" USB Ethernet adapter:

```text
[    4.162351] usb 1-1.2: new high-speed USB device number 4 using dwc_otg
[    4.263140] usb 1-1.2: New USB device found, idVendor=1a40, idProduct=0101, bcdDevice= 1.11
[    4.263161] usb 1-1.2: New USB device strings: Mfr=0, Product=1, SerialNumber=0
[    4.263178] usb 1-1.2: Product: USB 2.0 Hub
[    4.264153] hub 1-1.2:1.0: USB hub found
[    4.264339] hub 1-1.2:1.0: 4 ports detected
[    4.396021] random: crng init done
[    4.650296] usb 1-1.2.4: new high-speed USB device number 5 using dwc_otg
[    4.852139] usb 1-1.2.4: New USB device found, idVendor=0bda, idProduct=8152, bcdDevice=20.00
[    4.852158] usb 1-1.2.4: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[    4.852175] usb 1-1.2.4: Product: USB 10/100 LAN
[    4.852192] usb 1-1.2.4: Manufacturer: Realtek
[    4.852208] usb 1-1.2.4: SerialNumber: 00E04C360941
[    5.034312] usb 1-1.2.4: reset high-speed USB device number 5 using dwc_otg
[    5.266888] r8152 1-1.2.4:1.0 eth1: v1.10.11
```

```text
tc@piCorePlayer:~$ ifconfig
eth0      Link encap:Ethernet  HWaddr B8:27:EB:2C:47:29
          UP BROADCAST MULTICAST  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)

eth1      Link encap:Ethernet  HWaddr 00:E0:4C:36:09:41
          inet addr:192.168.1.114  Bcast:192.168.1.255  Mask:255.255.255.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:2703 errors:0 dropped:0 overruns:0 frame:0
          TX packets:568 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:185255 (180.9 KiB)  TX bytes:239065 (233.4 KiB)

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:2 errors:0 dropped:0 overruns:0 frame:0
          TX packets:2 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:1152 (1.1 KiB)  TX bytes:1152 (1.1 KiB)
```


## More information

- piCorePlayer expects network devices to be named either `eth0` or `wlan0`.
- Most things will probably work but there may be situations where the network devices have been hard coded as `eth0` or `wlan0`.
