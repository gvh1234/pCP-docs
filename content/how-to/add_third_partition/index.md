---
title: Add a third partition
description: Add a third partition on a SD card
date: 2020-12-10
author: pCP Team
weight: 100
pcpver: "7.0.0b6"
toc: true
draft: false
categories:
- Setup
- How to
tags:
- Partition
---

{{< lead >}}
If you need to add a third partition to a SD card it is recommended you use the pCP web GUI. This is a lot easier and will do the all calculations for you and will create a third partition in a consistent manner hopefully improving support. The "Manually add a third partition" instructions have been included for information only.
{{< /lead >}}

{{< card style="warning" >}}
- It is recommended that you use an additional USB storage device rather than adding a third partition to the SD card.
- Adding a third partition on a SD card will prevent resizing the second partition in the future.
- There is a chance that future upgrades may clobber the third partition.
{{< /card >}}

## Add a third partition

##### Step 1

pCP Web GUI: [Main Page] > [Resize FS]

Use the Web GUI's Resize FS command to increase the size of the second partition /dev/mmcblk0p2

##### Step 2

pCP Web GUI: [LMS] > "Add partition 3 (PCP_DATA)" > [Add]

##### Step 3

In the pCP Web GUI mount the third partition:

- Select [LMS].
- Check what FS Type is displaying in "Pick from the following detected USB disks to mount".
- Type mount point name (ie. /mnt/Music).
- Tick "Enabled" checkbox.
- Click [Set USB Mount].

##### Step 4

pCP Web GUI: [LMS] > "Create directories on partition 3 (PCP_DATA)" > [Create]


## Manually add a third partition

{{< card style="danger" >}}
Included for information only. It is recommended to use the pCP web GUI process above.
{{< /card >}}

##### Step 1

Access piCorePlayer via ssh---see [How-to access piCoreplayer via ssh](how_to_access_pcp_via_ssh).

##### Step 2

$ `fdisk -l /dev/mmcblk0`

```text
Disk /dev/mmcblk0: 7580 MB, 7948206080 bytes, 15523840 sectors
242560 cylinders, 4 heads, 16 sectors/track
Units: sectors of 1 * 512 = 512 bytes

Device       Boot StartCHS    EndCHS        StartLBA     EndLBA    Sectors  Size Id Type
/dev/mmcblk0p1    128,0,1     127,3,16          8192     139263     131072 64.0M  c Win95 FAT32 (LBA)
/dev/mmcblk0p2    128,0,1     127,3,16        139264     270335     131072 64.0M 83 Linux
```

This shows the partitions of a standard pCP SD card.

{{< card style="info" >}}
Note the second partition is the standard size of **64.0MB** which may be too small for adding additional extensions.
{{< /card >}}

##### Step 3

pCP Web GUI: [ Main Page ] > [ Resize FS ]

Use the Web GUI's Resize FS command to increase the size of the second partition /dev/mmcblk0p2

##### Step 4

$ `fdisk -l /dev/mmcblk0`

```text
Disk /dev/mmcblk0: 7580 MB, 7948206080 bytes, 15523840 sectors
242560 cylinders, 4 heads, 16 sectors/track
Units: sectors of 1 * 512 = 512 bytes

Device       Boot StartCHS    EndCHS        StartLBA     EndLBA    Sectors  Size Id Type
/dev/mmcblk0p1    128,0,1     127,3,16          8192     139263     131072 64.0M  c Win95 FAT32 (LBA)
/dev/mmcblk0p2    1023,3,16   1023,3,16       139264    1163263    1024000  500M 83 Linux
```

- Note the size of the second partition is now **500MB**.
- Note EndLBA of the second partition is, in this case, **1163263**.
- Do the maths **1163263+1=1163264**

##### Step 5

$ `fdisk /dev/mmcblk0`

```text
The number of cylinders for this disk is set to 242560.
There is nothing wrong with that, but this is larger than 1024,
and could in certain setups cause problems with:
1) software that runs at boot time (e.g., old versions of LILO)
2) booting and partitioning software from other OSs
   (e.g., DOS FDISK, OS/2 FDISK)

Command (m for help): n <== type
Partition type
   p   primary partition (1-4)
   e   extended
p <== type

Partition number (1-4): 3 <== type
First sector (16-15523839, default 16): 1163264 <== type
Last sector or +size{,K,M,G,T} (483328-15523839, default 15523839):
Using default value 15523839

Command (m for help): w <== type
The partition table has been altered.
Calling ioctl() to re-read partition table
fdisk: WARNING: rereading partition table failed, kernel still uses old table: Permission denied
```

A reboot will fix this.

##### Step 6

$ `pcp rb`

##### Step 7

Access piCorePlayer via ssh---see [Access piCoreplayer via ssh](/how-to/access_pcp_via_ssh/).

##### Step 8

$ `fdisk -l`

```text
Disk /dev/mmcblk0: 7580 MB, 7948206080 bytes, 15523840 sectors
242560 cylinders, 4 heads, 16 sectors/track
Units: sectors of 1 * 512 = 512 bytes

Device       Boot StartCHS    EndCHS        StartLBA     EndLBA    Sectors  Size Id Type
/dev/mmcblk0p1    128,0,1     127,3,16          8192     139263     131072 64.0M  c Win95 FAT32 (LBA)
/dev/mmcblk0p2    1023,3,16   1023,3,16       139264    1163263    1024000  500M 83 Linux
/dev/mmcblk0p3    1023,3,16   1023,3,16      1163264   15523839   14360576 7012M 83 Linux
Disk /dev/sda: 7633 MB, 8004304896 bytes, 15633408 sectors
244272 cylinders, 4 heads, 16 sectors/track
Units: sectors of 1 * 512 = 512 bytes

Device  Boot StartCHS    EndCHS        StartLBA     EndLBA    Sectors  Size Id Type
/dev/sda1    128,0,1     127,3,16          8192     139263     131072 64.0M  c Win95 FAT32 (LBA)
/dev/sda2    1023,3,16   1023,3,16       139264     344063     204800  100M 83 Linux
/dev/sda3    1023,3,16   1023,3,16       344064   15633407   15289344 7465M 83 Linux
```

##### Step 9

$ `sudo mkfs.ext4 /dev/mmcblk0p3`

```text
mke2fs 1.45.5 (07-Jan-2020)
Discarding device blocks: done
Creating filesystem with 1795072 4k blocks and 448800 inodes
Filesystem UUID: 5e2f8f42-7d92-4219-b36a-4eb7b0a06a5f
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): y <== type done
Writing superblocks and filesystem accounting information: done

```

##### Step 10

In the pCP Web GUI mount the third partition:

- Select [LMS].
- Check what FS Type is displaying in "Pick from the following detected USB disks to mount".
- Type mount point name (ie. /mnt/Music).
- Tick "Enabled" checkbox.
- Click [Set USB Mount].


## Delete third partition

##### Step 1

Access piCorePlayer via ssh---see [Access piCoreplayer via ssh](/how-to/access_pcp_via_ssh/).

##### Step 2

$ `fdisk /dev/mmcblk0`

```text
The number of cylinders for this disk is set to 242560.
There is nothing wrong with that, but this is larger than 1024,
and could in certain setups cause problems with:
1) software that runs at boot time (e.g., old versions of LILO)
2) booting and partitioning software from other OSs
   (e.g., DOS FDISK, OS/2 FDISK)

Command (m for help): d <== type
Partition number (1-4): 3 <== type

Command (m for help): p <== type
Disk /dev/mmcblk0: 7580 MB, 7948206080 bytes, 15523840 sectors
242560 cylinders, 4 heads, 16 sectors/track
Units: sectors of 1 * 512 = 512 bytes

Device       Boot StartCHS    EndCHS        StartLBA     EndLBA    Sectors  Size Id Type
/dev/mmcblk0p1    128,0,1     127,3,16          8192     139263     131072 64.0M  c Win95 FAT32 (LBA)
/dev/mmcblk0p2    1023,3,16   1023,3,16       139264    1163263    1024000  500M 83 Linux

Command (m for help): w <== type
The partition table has been altered.
Calling ioctl() to re-read partition table
fdisk: WARNING: rereading partition table failed, kernel still uses old table: Permission denied
```

##### Step 3

$ `pcp rb`


## More information

- [SD Card Formatter](https://www.sdcard.org/downloads/formatter_4/)
