---
title: Access pCP via ssh
description: Access piCorePlayer via ssh
date: 2020-08-19T00:00:00.000Z
author: pCP Team
weight: 50
pcpver: 5.0.0
toc: true
draft: false
categories:
  - Setup
  - How to
tags:
  - ssh
---

## From the command line

##### Step 1
Type `ssh user@<IP address>` at the command prompt.

$ `ssh tc@192.168.1.111`

##### Step 2
The first time you access the remote computer, type `yes` to establish host authenticity.
```
The authenticity of host '<IP address> (<IP address>)' can't be established.
ECDSA key fingerprint is SHA256:Oj3eagEfJYeltdxRbmNsVmqPDF4SO4m7KmVl8+3KB5A.
Are you sure you want to continue connecting (yes/no)?
```
Are you sure you want to continue connecting (yes/no)? `yes`

##### Step 3
Type piCorePlayer's password when prompted.

tc@<IP address>'s password: `piCore`

##### Step 4

Type `exit` to close terminal.


## Windows

##### Step 1

Start your ssh client software (ie. PuTTY).

![PuTTY screen](putty.png)

##### Step 2

Enter your IP address.

##### Step 3

Set Port to 22.

##### Step 4

Select SSH.

##### Step 5

Click [Save].

##### Step 6

Click [Open].

##### Step 7

Login into your piCorePlayer.

login as: `tc`<br>
password: `piCore` (default)

![piCore login terminal](putty_login.png)

##### Step 8

Type `exit` or click [X] to close terminal.


## More information

- [piCorePlayer aliases](/information/pcp_aliases/)
- [piCorePlayer CLI](/information/pcp_cli/)
- [Determine your pCP IP address](/how-to/determine_your_pcp_ip_address/)
