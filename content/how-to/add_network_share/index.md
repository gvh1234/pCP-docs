---
title: Add a network share
description: Add a network share (Synology NAS)
date: 2020-12-04
author: redgum
weight: 115
pcpver: "6.1.0"
toc: true
draft: false
categories:
- Setup
- How to
tags:
- LMS
- share
---

{{< lead >}}
I'm assuming that you are a Linux noob, like me, and want to connect to a Synology NAS.
{{< /lead >}}
{{< lead >}}
You have to configure access at both the NAS and the piCorePlayer LMS.
{{< /lead >}}

For the piCorePlayer LMS you need:

1. A name for the mount point. This will only be used by the piCorePlayer LMS, so I used the name of my Synology NAS.
2. The IP address for the NAS on your local network. 
3. The share name from your NAS. You will need to get this from Shared Folders on your NAS, in my case it is '/Volume 1/Music'
4. Select NFS as the Share Type.

{{< card style="warning" >}}
Don't press the 'Set NET Mount' button yet!
{{< /card >}}

Then on your Synology NAS you:

1. Go to 'File Services' and enable the NFS checkbox.
2. Go to 'Shared Folder', right click and select Edit, then go to permissions, then select the folders, and then click 'NFS Permissions'.

For the NAS configuration---see [How to access files on Synology NAS within the local network (NFS)](https://www.synology.com/en-global/knowledgebase/DSM/tutorial/File_Sharing/How_to_access_files_on_Synology_NAS_within_the_local_network_NFS).

You will require the host name of the piCorePlayer LMS---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).

Then go back to the piCorePlayer LMS and press the 'Set NET Mount' button.

All done!


## More information

- [How to access files on Synology NAS within the local network (NFS)](https://www.synology.com/en-global/knowledgebase/DSM/tutorial/File_Sharing/How_to_access_files_on_Synology_NAS_within_the_local_network_NFS)
