---
title: Setup Wifi
description:
date: 2020-12-05
author: SBP
weight: 60
pcpver: "4.0.0"
toc: true
draft: false
categories:
- Setup
- How to
tags:
- Wifi
---

##### Step 1

Click on [Wifi Settings].

##### Step 2

Select "Set Wifi configuration" > Wifi > "On".

Click [Save].

This will refresh the [Wifi Settings] page with all the options enabled.

![Initial setup](wifi1.jpg)

##### Step 3

Click "Wifi information" > [Scan] to search for available networks.

![Scan for network](wifi3.jpg)

##### Step 4

Note the SSID of your Wifi network (or highlight with mouse and copy (ctrl-c)).

![Select Wifi](wifi4.jpg)

##### Step 5

Fill in the "Set Wifi configuration" fields.

Type your "SSID" (or paste (ctrl-v)).

Type your "PSK Password".

Type your "Country Code".

Select your "Security Mode".

Click [Save] to save your Wifi settings.

{{< card style="info" >}}Your "PSK Password" will be converted to a secure "PSK Passphrase".{{< /card >}}

![Wifi settings](wifi5.jpg)

##### Step 6

piCorePlayer should now be connected to your wifi network.

Check "Wifi information" > "Wifi IP" for the IP address being used.

![Wifi IP address](wifi6.jpg)

##### Step 7

Click [Main Page].

Click [Reboot].

Quickly remove the LAN cable.

When piCorePlayer has rebooted, you should be connected via Wifi using the Wifi IP address.


## More information

- [Setup Wifi without ethernet](/how-to/setup_wifi_on_pcp_without_ethernet/)
- [Wifi does not work](/faq/wifi_does_not_work/)
- [Example wpa_supplicant configuration file](http://w1.fi/cgit/hostap/plain/wpa_supplicant/wpa_supplicant.conf)
- [WPA Supplicant](http://w1.fi/wpa_supplicant/)
- [USB wifi adapters - historical information](https://sites.google.com/site/picoreplayer/wifi-dongles)
