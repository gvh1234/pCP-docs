---
title: Install LMS
description: Install Logitech Media Server (LMS)
date: 2020-11-27
author: nowhinjing
weight: 80
pcpver: "3.5.0"
draft: false
toc: true
categories:
- Setup
- How to
tags:
- LMS
---

## 1. Connect the Raspberry Pi and Initial Configuration

Insert the SD card into your Raspberry Pi, attach a wired ethernet connection, and plug in the power. 
Give your Raspberry Pi a minute or so to power up and connect to your local network.

Then look at the boot console. The IP address will be displayed at the end of the boot process.

![Boot screen](pcp_ip_address_bootlog.png)

Or launch **Advanced IP Scanner** to identify the IP address that has automatically been assigned by your network to the Raspberry Pi via DHCP.

![Find pCP IP address](LMS_03.png)

Fire up your favourite browser, and enter the IP address you found from the previous step. You should now see the **pCP** welcome screen.

![pCP welcome page](LMS_04.png)

Select [Main Page] and then [Beta]. Note the warning that is given !

![Select Beta mode](LMS_05.png)

Move down to "Advanced mode operations" and select [Resize FS].

![Select Resize](LMS_06.png)

Select "Use 1000 MB" and hit the resize button.

{{< card title="Note" style="danger" >}}If you have a large library select "Use 2000 MB" or "Whole SD card".{{< /card >}}

![resize filesystem](LMS_07.png)

pCP will go away for a while to do it's thing, but eventually it should refresh and you should be back on the [Main Page].


## 2. Install LMS

Select [LMS] from the [Main Page] menu bar, and then click on [Install] in "Logitech Media Server (LMS) operations".

![install lms](LMS_08.png)

After installation LMS is not automatically started, so start it by selecting the [Start] item in "Logitech Media Server (LMS) operations".

![start lms](LMS_09.png)


## 3. Tips and Tweaks

### 3.1 Turn Off the Internal Player

Select [Tweaks] from the menu bar, scroll down to the "Audio tweaks" section and set "Automatically start Squeezelite when pCP starts" to "No" and then hit [Save]. Reboot the Raspberry Pi for the changes to take effect.

![switch off squeezelite](LMS_21.png)

### 3.2 Show piCoreLMS rather than piCorePlayer as the name

Select [Tweaks] from the menu bar, change the "Host name" to your required value and then hit [Save]. Reboot the Raspberry Pi for the changes to take effect.

![change name](LMS_22.png)

### 3.3 Set a Fixed IP Address

As this is an 'always-on' server providing music to the entire household, I have chosen to give it a fixed IP Address rather than an address automatically assigned by your router via DHCP.

IF YOU DON'T FULLY UNDERSTAND THE PRINCIPLES BEHIND THIS, PLEASE DO NOT ATTEMPT IT.

Select [Main Page] from the menu bar, scroll down to the "Beta mode operation" section and select [Static IP].

![set static ip](LMS_23.png)

Enter the required information and then hit [Save].

![pcp set fixed ip address](LMS_24.png)

Return to the [Main Page] and reboot the Raspberry Pi for the changes to take effect.

Re-running **Advanced IP Scanner** will show the new fixed IP address.

![ip scan fixed address](LMS_25.png)


## More information

- [Advanced IP Scanner - Windows](http://www.advanced-ip-scanner.com/)
- [nmap - Linux, Mac OS X and Windows](https://nmap.org/)
