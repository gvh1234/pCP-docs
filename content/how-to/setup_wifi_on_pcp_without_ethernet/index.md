---
title: Setup Wifi without ethernet
description:
date: 2020-12-05
author: pCP Team
weight: 70
pcpver: "5.0.0"
toc: true
draft: false
categories:
- Setup
- How to
tags:
- Wifi
---

There are situations where these methods will be useful:

- You have a Raspberry Pi without built-in ethernet (ie. RPi0, RPi0W, RPiA, RPiA+ ).
- You have a Raspberry Pi with built-in Wifi (ie. RPi0W ,RPi3B, RPi3B+, RPi4B).
- Wired Ethernet is not available.


## Add wpa_supplicant.conf to boot partition on PC

##### Step 1

Burn image to a SD card---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).

{{< card style="info" >}}Do not eject the SD card as you need to add "wpa_supplicant.conf" to the boot partition.{{< /card >}}

##### Step 2

Create a new file, or download a sample [wpa_supplicant.conf](https://www.picoreplayer.org/sample_files/wpa_supplicant.conf) and modify.

- Used for quick setup at boot.
- Can be later maintained via pCP web interface.
- Do not use special characters.

Type or copy one of the following using your text editor (ie. Notepad++).

##### Maintained by piCorePlayer

{{< code >}}# Maintained by piCorePlayer
ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=staff
# Two Character Country Code
country=US
update_config=1

network={
	ssid="yourssid"
	psk="password"
	key_mgmt=WPA-PSK
	auth_alg=OPEN
}{{< /code >}}

##### Maintained by user

- User maintains by placing this on the boot disk to update.
- pCP will not modify in any way.
- Special Characters are allowed.

{{< code >}}# Maintained by user
ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=staff
# Two Character Country Code
country=US
update_config=1

network={
	ssid="Your SSID"
	psk="P@ssword"
	key_mgmt=WPA-PSK
	auth_alg=OPEN
}{{< /code >}}

##### Step 3

[Save As] the file "DRIVE:\wpa_supplicant.conf" to the boot partition.

On a PC, DRIVE will be the drive letter for your SD card. (ie. F:)

##### Step 4

Eject SD card.

##### Step 5

Insert SD card into piCorePlayer.

##### Step 6

Apply power.


## Using setup with keyboard and monitor

{{< card style="warning" >}}Setup is not always up-to-date.{{< /card >}}

##### Step 1

Login into your piCorePlayer.

login as: `tc`

password: `piCore` (default)

![piCore login terminal](wifi_putty_login.png)

##### Step 2

Type `setup`.

![Setup - Important message](setup_important_message.png)

##### Step 3

Click [Continue].

![Setup - Main Menu](setup_main_menu.png)

##### Step 4

Click [OK].

![Setup - Wifi Status Menu](setup_wifi_status_menu.png)

##### Step 5

Click [No].

![Setup - Wifi On Off Menu](setup_wifi_on_off_menu.png)

##### Step 6

Click [On].

![Setup - Wifi Menu](setup_wifi_menu.png)

##### Step 7

Special Characters are not permitted.

Enter SSID.

Enter Password.

Enter Security.

Click [OK].

![Setup - Wifi Status Menu](setup_wifi_new_status_menu.png)

##### Step 8

Click [Yes].

##### Step 9

Click [Exit].

##### Step 10

Type `pcp br`.


## More information

- [Setup Wifi](/how-to/setup_wifi/)
- [Wifi does not work](/faq/wifi_does_not_work/)
- [Example wpa_supplicant configuration file](http://w1.fi/cgit/hostap/plain/wpa_supplicant/wpa_supplicant.conf)
- [WPA Supplicant](http://w1.fi/wpa_supplicant/)
- [USB wifi adapters - historical information](https://sites.google.com/site/picoreplayer/wifi-dongles)
