---
title: Burn pCP onto a SD card
description: Burn piCorePlayer onto a SD card
date: 2020-10-07
author: pCP Team
weight: 20
pcpver: "6.1.0"
toc: true
draft: false
categories:
- Setup
- How to
tags:
- SD card
---

{{< lead >}}
There are many methods of creating a piCorePlayer SD card. You can use your favourite method or use one of the following.
{{< /lead >}}

## Windows

- [USBImager](/how-to/burn_pcp_onto_a_sd_card/windows/usbimager/)
- [Win32 Disk Imager](/how-to/burn_pcp_onto_a_sd_card/windows/win32diskimager/)
- [Raspberry Pi Imager](https://www.raspberrypi.org/blog/raspberry-pi-imager-imaging-utility/)

## Linux

- [USBImager - GitLab](https://gitlab.com/bztsrc/usbimager#usbimager)
- [Using dd command](/how-to/burn_pcp_onto_a_sd_card/linux/dd/)
- [Raspberry Pi Imager](https://www.raspberrypi.org/blog/raspberry-pi-imager-imaging-utility/)

## Mac OSX

- [USBImager - GitLab](https://gitlab.com/bztsrc/usbimager#usbimager)
- [ApplePi-Baker](https://www.tweaking4all.com/hardware/raspberry-pi/applepi-baker-v2/)
- [Raspberry Pi Imager](https://www.raspberrypi.org/blog/raspberry-pi-imager-imaging-utility/)


## More information

- [RPF - Installing Operating System Images](https://www.raspberrypi.org/documentation/installation/installing-images/)
- [eLinux - RPi Easy SD Card Setup](http://elinux.org/RPi_Easy_SD_Card_Setup)
- [Win32 Disk Imager - Sourceforge](https://sourceforge.net/projects/win32diskimager/)
- [USBImager - GitLab](https://gitlab.com/bztsrc/usbimager#usbimager)
- [ApplePi-Baker](https://www.tweaking4all.com/hardware/raspberry-pi/applepi-baker-v2/)
- [Raspberry Pi Imager](https://www.raspberrypi.org/blog/raspberry-pi-imager-imaging-utility/)
- [Raspberry Pi Imager - Downloads](https://www.raspberrypi.org/downloads/)
- [SD Card Formatter](https://www.sdcard.org/downloads/formatter_4/)
- [Can't see the boot partition](/faq/cant_see_boot_partition/)
- [Can't see the root partition](/faq/cant_see_root_partition/)
