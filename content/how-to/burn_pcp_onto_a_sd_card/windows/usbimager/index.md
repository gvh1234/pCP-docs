---
title: USBImager
description:
date: 2020-11-05
author: pCP Team
weight: 10
pcpver: "6.1.0"
toc: true
draft: false
hidden: true
categories:
- Setup
- How to
tags:
- SD card
---

{{< lead >}}
USBImager is a really really simple GUI application that writes compressed disk images to USB drives
and creates backups. Available platforms: Windows, MacOSX and Linux. Its interface is as simple as it gets, totally bloat-free.
{{< /lead >}}

##### Step 1

Insert SD card into PC.

##### Step 2

Click [Cancel] if prompted to format disk.

{{< card style="danger" >}}Never format the SD card even if prompted.{{< /card >}}

![Windows Format Drive](usbimager_format_disk.png)

{{< card style="danger" >}}Depending on software loaded on your PC, you may be get a series of messages when you plug your SD card in.
- Ignore formatting requests.
- Ignore anti-virus warnings.
- Ignore requests to do backups.{{< /card >}}

##### Step 3

Note the new drive letter that has been created (ie. D:).

##### Step 4

Click [USBImager].

![USBImager - Blank](usbimager_blank.png)

##### Step 5

Select Image File (ie. piCorePlayer6.1.0.img).

![Win32 Disk Imager - Image File](usbimager_image_file.png)

##### Step 6

Set Device to your SD card (ie. D:).

{{< card style="danger" >}} Ensure you have your SD card selected. You can accidentally overwrite USB drive! {{< /card >}}

![Win32 Disk Imager - Device](usbimager_device.png)

##### Step 7

{{< card style="warning" >}}Ensure Target Device is correct.{{< /card >}}
{{< card style="warning" >}}Ensure Target Device label is correct.{{< /card >}}

Write data on "Image file" to "Device".

Click [Write].

![Win32 Disk Imager - Write](usbimager_write.png)

##### Step 8

In progress.

![Win32 Disk Imager - In Progress](usbimager_in_progress.png)

##### Step 9

Writing should take about 14 seconds.

Exact time will depend on SD card quality.

![Win32 Disk Imager - Complete](usbimager_complete.png)

##### Step 10

Click [X] to exit program.

![Win32 Disk Imager - Exit](usbimager_exit.png)

##### Step 11

Click [Safely Remove Hardware and Eject Media].

##### Step 12

Click [Eject SD Card].

![Win32 Disk Imager - Eject SD card](usbimager_eject_sd_card.png)


## More information

- [USBImager - GitLab](https://gitlab.com/bztsrc/usbimager#usbimager)
