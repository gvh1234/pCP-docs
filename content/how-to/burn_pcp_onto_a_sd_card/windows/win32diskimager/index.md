---
title: Win32 Disk Imager
description: Using Win32 Disk Imager
date: 2020-11-05
author: pCP Team
weight: 20
pcpver: "6.1.0"
toc: true
draft: false
hidden: true
categories:
- Setup
- How to
tags:
- SD card
---

{{< lead >}}
This program is designed to write a raw disk image to a removable device or backup a removable device to a raw image file. It is very useful for embedded development, namely Arm development projects (Android, Ubuntu on Arm, etc).
{{< /lead >}}

##### Step 1

Insert SD card into PC.

##### Step 2

Click [Cancel] if prompted to format disk.

{{< card style="danger" >}}Never format the SD card even if prompted.{{< /card >}}

![Windows Format Drive](win32diskimager_format_disk.png)

{{< card style="danger" >}}Depending on software loaded on your PC, you may be get a series of messages when you plug your SD card in.
- Ignore formatting requests.
- Ignore anti-virus warnings.
- Ignore requests to do backups.{{< /card >}}

##### Step 3

Note the new drive letter that has been created (ie. F:).

##### Step 4

Click [Win32 Disk Imager].

![Win32 Disk Imager - Blank](win32diskimager_blank.png)

##### Step 5

Select Image File (ie. piCorePlayer3.11.img).

![Win32 Disk Imager - Image File](win32diskimager_image_file.png)

##### Step 6

(Optional) Click [MD5 Hash].

Compare MD5 checksum with contents of piCoreImagex.xx.img.md5.txt

![Win32 Disk Imager - MD5 Hash](win32diskimager_md5_hash.png)

##### Step 7

Set Device to your SD card (ie. F:).

{{< card style="danger" >}} Ensure you have your SD card selected. You can accidently overwrite USB drive! {{< /card >}}

![Win32 Disk Imager - Device](win32diskimager_device.png)

##### Step 8

Click [Write].

Write data on "Image file" to "Device".

![Win32 Disk Imager - Write](win32diskimager_write.png)

##### Step 9

Read Confirm overwrite message.

{{< card style="warning" >}}Ensure Target Device is correct.{{< /card >}}
{{< card style="warning" >}}Ensure Target Device label is correct.{{< /card >}}

Click [Yes].

![Win32 Disk Imager - Confirm Overwrite](win32diskimager_confirm_overwrite.png)

##### Step 10

Writing should take about 10 seconds.

Exact time will depend on SD card quality.

Click [OK].

![Win32 Disk Imager - Complete](win32diskimager_complete.png)

##### Step 11

Click [Exit].

![Win32 Disk Imager - Exit](win32diskimager_exit.png)

##### Step 12

Click [Safely Remove Hardware and Eject Media].

##### Step 13

Click [Eject Device] pCP (F:).

![Win32 Disk Imager - Eject SD card](win32diskimager_eject_sd_card.png)


## More information

- [Win32 Disk Imager](https://sourceforge.net/projects/win32diskimager/)
