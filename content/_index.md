---
title: piCorePlayer documentation
description:
date: 2020-08-05
author: pCP Team
weight: 1
pcpver: "6.1.0"
toc: false
draft: false
---

## Welcome

{{< lead >}}
Our first attempt at Online Help was not as successful as expected, but it was a valuable lesson. So let's try again. Documentation is important but if it is a chore it will not get done. So this time, let's focus on simplicity and speed. Remove the barriers and it will happen.
{{< /lead >}}

## Tools
<div class="row py-3 mb-5">
	<div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="fas fa-tachometer-alt fa-2x text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					Hugo
				</h5>
				<p class="card-text text-muted">
					Static files generated in a second. Local staging for testing. Source submitted to GitLab gets automatically pushed to production using Hugo.
				</p>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="fab fa-markdown fa-2x text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					Markdown
				</h5>
				<p class="card-text text-muted">
					Just enough formatting while remaining simple. User can submit markdown or text files.
				</p>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="fab fa-git-square fa-2x text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					Git
				</h5>
				<p class="card-text text-muted">
					Source managed by git. There's no better solution.
				</p>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="fab fa-bootstrap fa-2x text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					Bootstrap
				</h5>
				<p class="card-text text-muted">
					Fully responsive using Bootstrap 4. Works on small screens.
				</p>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="fas fa-file fa-2x text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					ACE documentation theme
				</h5>
				<p class="card-text text-muted">
					A great Hugo theme using Bootstrap 4. Ideal starting point for creating documentation.
				</p>
			</div>
		</div>
	</div>
</div>
